<?php

namespace App\Exports;

use App\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsersExport implements FromCollection, ShouldAutoSize, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return  User::select(
            'users.id',
            'users.name',
            'users.last_name',
            'users.email',
            'users.created_at',
            DB::raw("CAST(COUNT(DISTINCT buckettable.user_id) AS CHAR) as bucket_count"),
            DB::raw("CAST(status AS CHAR) as status")
        )
            ->leftJoin('buckettable', 'buckettable.user_id', '=', 'users.id')
            ->groupBy('users.id')
            ->get();
    }
    public function headings(): array
    {
        return [
            'ID',
            'Vorname',
            'Name',
            'Email',
            'Register Datum',
            'Anzahl Merkliste',
            'Status',
        ];
    }
}
