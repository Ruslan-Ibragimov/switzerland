<?php

namespace App;

use App\Filters\UserFilter as Filters;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasRoles, EntryFilter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function booted()
    {
        parent::booted(); // TODO: Change the autogenerated stub

        static::deleting(function ($model) {
            $model->bucket->each->delete();
        });
    }

    /**
     * Get user all bucket list.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bucket()
    {
        return $this->hasMany(Bucket::class);
    }

    /**
     * Apply all relevant user filters.
     *
     * @param  Builder $query
     * @param \App\Filters\UserFilter $filters
     * @return Builder
     */
    public function scopeFilter($query, Filters $filters)
    {
        return $filters->apply($query);
    }
}
