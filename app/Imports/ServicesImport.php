<?php

namespace App\Imports;

use App\Category;
use App\Event;
use App\Place;
use App\Service;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ServicesImport implements ToCollection
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection  $rows)
    {
        foreach ($rows as $index => $row) {
            try {
                if ($index === 0) continue;
                if ($row[0] !== null) {

                    // $event = new Event();
                    // $event->category_id = Category::where('name_de', 'like', '%' . $row[2]  . '%')->first()->id;
                    // // $event->category_id = $row[2];
                    // $event->title = $row[3];
                    // $event->description_de = $row[4];
                    // $event->description_en = $row[5];
                    // $event->tickets = $row[6];
                    // $event->facebook = $row[7];
                    // $event->instagram = $row[8];
                    // $event->video_link = $row[9];
                    // $event->app_tipp = $row[10];
                    // $event->sample_date = $row[11];
                    // $event->address = $row[13];
                    // $event->google = $row[14] . ',' . $row[15];
                    // $event->preview = 'uploads/events/' . $row[16];
                    // $event->valid_from = date('Y-m-d');
                    // $event->valid_to = date('Y-m-d');
                    // $event->topic_id = 2;
                    // $event->save();




                    // Must See
                    // $place = new Place();
                    // $place->region_id = $row[2];
                    // $place->title = $row[4]  != 'n/a' ? $row[4] : null;
                    // $place->video_link  = $row[10]  != 'n/a' ? $row[10] : null;
                    // $place->preview  = 'uploads/places/' . $row[17] . ".jpg";
                    // $place->address  = $row[14]  != 'n/a' ? $row[14] : null;
                    // $place->google  = $row[16] . ',' . $row[15]  != 'n/a' ? $row[16] . ',' . $row[15] : null;
                    // $place->description_de  = $row[12]  != 'n/a' ? $row[12] : null;
                    // $place->description_en  = $row[13]  != 'n/a' ? $row[13] : null;
                    // $place->weblink  = $row[7]  != 'n/a' ? $row[7] : null;
                    // $place->facebook  = $row[8]  != 'n/a' ? $row[8] : null;
                    // $place->instagram  = $row[9]  != 'n/a' ? $row[9] : null;
                    // $place->app_tipp  = $row[11]  != 'n/a' ? $row[11] : null;
                    // $place->category_id  = $row[6];
                    // $place->topic_id  = 1;
                    // $place->save();

                    //Services
                    // $service = new Service();
                    // $service->title_de =  $row[1];
                    // $service->preview =  'uploads/services/' . $index . '.jpg';
                    // $service->title_en =  $row[2];
                    // $service->description_de =  $row[3];
                    // $service->description_en =  $row[4];
                    // $service->weblink_de =  $row[5];
                    // $service->weblink =  $row[6];
                    // $service->app_tipp_de =  $row[7];
                    // $service->app_tipp =  $row[8];
                    // $service->topic_id = 4;
                    // $service->save();
                }
            } catch (\Throwable $th) {
                echo $th;
                dump($th);
                dd($row);
            }
        }
        return $rows;
    }
}
