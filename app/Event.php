<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use Categoriable, RecordsBucket, EntryFilter, RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_link',
        'preview',
        'title',
        'address',
        'google',
        'description_en',
        'description_de',
        'tickets',
        'facebook',
        'instagram',
        'app_tipp',
        'app_tipp_android',
        'phone',
        'topic_id',
        'category_id',
        'valid_from',
        'valid_to'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['categories'];

    /**
     * Get topic of the entry.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
