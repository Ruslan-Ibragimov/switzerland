<?php

namespace App;

use \App\Filters\EntryFilter as Filters;

trait EntryFilter
{
    /**
     * Apply all relevant entries filters.
     *
     * @param  Builder $query
     * @param \App\Filters\EntryFilter $filters
     * @return Builder
     */
    public function scopeFilter($query, Filters $filters)
    {
        return $filters->apply($query);
    }
}
