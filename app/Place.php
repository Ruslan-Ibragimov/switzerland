<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use Categoriable, RecordsBucket, EntryFilter, RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_link',
        'preview',
        'title',
        'address',
        'google',
        'description_en',
        'description_de',
        'weblink',
        'facebook',
        'instagram',
        'app_tipp',
        'app_tipp_android',
        'phone',
        'region_id',
        'topic_id',
        'category_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['categories', 'region'];

    /**
     * Get region of the place.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }
    /**
     * Get topic of the entry.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
