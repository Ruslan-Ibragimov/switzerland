<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('check_email', function ($attribute, $value, $parameters, $validator) {
            $user = \App\User::where('email', $value)->first();
            return isset($user->id);
        });
        Validator::extend('unique_email', function ($attribute, $value, $parameters, $validator) {
            $user = \App\User::where('email', $value)->where('status', 1)->first();
            return !isset($user->id);
        });
        Validator::extend('check_coords', function ($attribute, $value, $parameters, $validator) {
            $request = app(\Illuminate\Http\Request::class);
            $coords = double_coords($value);
            if (!$coords) return false;
            $request[$attribute] = $coords;
            return true;
        }, 'Google coordinates must be separated by one comma,   eg: 3231.21, -43223');
    }
}
