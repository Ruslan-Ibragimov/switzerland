<?php

function double_coords($data)
{
    $data = str_replace(' ', '', $data);
    $coords = explode(',', $data);
    if (count($coords) != 2) {
        return false;
    }
    return fiX_to_coord($coords[0]) . ', ' . fiX_to_coord($coords[1]);
}

function fiX_to_coord($numb)
{
    if ((string) (int) $numb == $numb) {
        $numb = intval($numb);
        $numb = number_format($numb, 1, '.', '');
    }
    return $numb;
}
