<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bucket extends Model
{
    use  EntryFilter;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'buckettable';

    /**
     * Don't auto-apply mass assignment protection.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Fetch the associated enrty for the bucket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entry()
    {
        return $this->morphTo();
    }

    /**
     * Fetch an bucket list for the given user.
     *
     * @param  User $user
     * @param  int  $take
     * @return \Illuminate\Database\Eloquent\Collection;
     */
    public static function bucket($user, $take = 50)
    {
        return static::where('user_id', $user->id)
            ->latest()
            ->take($take);
    }
}
