<?php

namespace App\Http\Middleware;

use Closure;

class CheckLocalization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = $request->header('lang');
        if ($lang) app()->setLocale($lang);
        if ($lang && $lang === 'de') setlocale(LC_TIME, 'de_DE', 'deu_deu');
        // setlocale(LC_TIME, "de_DE");
        return $next($request);
    }
}
