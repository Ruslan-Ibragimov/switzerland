<?php

namespace App\Http\Requests\Api;


class AuthRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->named('api.login')) {
            return $this->loginRules();
        } else if ($this->route()->named('api.register')) {
            return $this->registerRules();
        } else if ($this->route()->named('api.forget')) {
            return $this->forgetRules();
        } else {
            return [];
        }
    }
    public function forgetRules()
    {
        return [
            'email' => 'required|check_email',
        ];
    }
    public function loginRules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    public function registerRules()
    {
        return [
            'last_name' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique_email',
            'password' => 'required|string|min:8|confirmed',
            'device_name' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'email.unique_email' => __('auth.unique_email'),
            'email.check_email' => __('auth.check_email'),
        ];
    }
}
