<?php

namespace App\Http\Requests\Api;


class SupportRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'smartphone' => 'required|string|max:255',
            'system' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'note' => 'required|string',
        ];
    }
}
