<?php

namespace App\Http\Requests\Api;


class EntryRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->named('api.events.store')) {
            return $this->eventStore();
        } else if ($this->route()->named('api.places.store')) {
            return $this->placeStore();
        } else if ($this->route()->named('api.services.store')) {
            return $this->serviceStore();
        } else if ($this->route()->named('api.shops.store')) {
            return $this->shopsStore();
        } else if ($this->route()->named('api.tips.store')) {
            return $this->tipsStore();
        } else if ($this->route()->named('api.bucket-export')) {
            return $this->importBucket();
        } else {
            return [];
        }
    }
    public function importBucket()
    {
        return [
            'email' => 'required|string|email|max:255'
        ];
    }
    public function eventStore()
    {
        return [
            'id' => 'required|exists:events'
        ];
    }
    public function placeStore()
    {
        return [
            'id' => 'required|exists:places'
        ];
    }
    public function serviceStore()
    {
        return [
            'id' => 'required|exists:services'
        ];
    }
    public function shopsStore()
    {
        return [
            'id' => 'required|exists:shops'
        ];
    }
    public function tipsStore()
    {
        return [
            'id' => 'required|exists:tips'
        ];
    }
}
