<?php

namespace App\Http\Controllers;

use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Service;
use App\Region;
use App\Topic;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    use ImageUploader, EntryFilterControllerExtend;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $services = $this->filter(Service::class, $filters)->paginate(25);
        $categories = Topic::where('name_en', 'Services')->firstOrFail()->categories()->get();

        return view('admin.service.index', compact(['services', 'categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::all();
        $categories = Topic::where('name_en', 'Services')->firstOrFail()->categories()->get();

        return view('admin.service.create', compact(['regions', 'categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'preview' => 'mimes:jpg,jpeg|dimensions:min_width=1120,min_height=610',
            // 'category_id' => 'required|exists:categories,id',
            'title_en' => 'required',
            'title_de' => 'required',
            'description_en' => 'required',
            'description_de' => 'required'
        ]);
        if ($request->video_link) {
            $request->validate([
                'video_link' => 'url',
            ]);
        }
        // Get topic
        $topic = Topic::where('name_en', 'Services')->firstOrFail();

        // Uplaod preview image
        $preview = $this->upload($request->preview);

        // Create new entry
        $service = Service::create([
            'video_link' => $request->video_link,
            'preview' => $preview,
            'title_en' => $request->title_en,
            'title_de' => $request->title_de,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'weblink' => $request->weblink,
            'weblink_de' => $request->weblink_de,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'app_tipp' => $request->app_tipp,
            'app_tipp_de' => $request->app_tipp_de,
            'app_tipp_android' => $request->app_tipp_android,
            'app_tipp_android_de' => $request->app_tipp_android_de,
            'phone' => $request->phone,
            'topic_id' => $topic->id,
            // 'category_id' => $request->category_id
        ]);
        return redirect()->route('service.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $regions = Region::all();
        $categories = Topic::where('name_en', 'Services')->firstOrFail()->categories()->get();

        return view('admin.service.edit', compact(['regions', 'categories', 'service']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $request->validate([
            // 'category_id' => 'required|exists:categories,id',
            'title_en' => 'required',
            'title_de' => 'required',
            'description_en' => 'required',
            'description_de' => 'required',
        ]);

        $preview = $service->preview;

        if ($request->video_link) {
            $request->validate([
                'video_link' => 'url',
            ]);
        }
        if ($request->preview) {
            $request->validate([
                'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=1120,min_height=610',
            ]);

            $preview = $this->upload($request->preview, true, $preview);
        }

        $service->update([
            'video_link' => $request->video_link,
            'preview' => $preview,
            'title_en' => $request->title_en,
            'title_de' => $request->title_de,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'weblink' => $request->weblink,
            'weblink_de' => $request->weblink_de,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'app_tipp' => $request->app_tipp,
            'app_tipp_de' => $request->app_tipp_de,
            'app_tipp_android' => $request->app_tipp_android,
            'app_tipp_android_de' => $request->app_tipp_android_de,
            'phone' => $request->phone,
            // 'category_id' => $request->category_id
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Service $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Service $service)
    {
        $service->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('service.index');
    }

    /**
     * Set disabled status.
     *
     * @param Service $service
     */
    public function diactivate(Service $service)
    {
        $service->status = !$service->status;
        $service->save();

        return back();
    }
}
