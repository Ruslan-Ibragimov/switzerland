<?php

namespace App\Http\Controllers;

use App\Author;
use App\City;
use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Tip;
use App\Topic;
use Illuminate\Http\Request;

class TipController extends Controller
{
    use ImageUploader, EntryFilterControllerExtend;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $tips = $this->filter(Tip::class, $filters)->paginate(25);
        $categories = Topic::where('name_en', 'Tips From Locals')->firstOrFail()->categories()->get();

        return view('admin.tip.index', compact(['tips', 'categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authors = Author::latest()->get();
        $cities = City::all();
        $categories = Topic::where('name_en', 'Tips From Locals')->firstOrFail()->categories()->get();

        return view('admin.tip.create', compact(['authors', 'categories', 'cities']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=512,min_height=800',
            'author_id' => 'required|exists:authors,id',
            'category_id' => 'required|exists:categories,id',
            'city_id' => 'required|exists:cities,id',
            'title' => 'required',
            'address' => 'required',
            'google' => 'required|check_coords',
            'description_en' => 'required',
            'description_de' => 'required',
            'weblink' => 'required|url',
        ]);

        $topic = Topic::where('name_en', 'Tips From Locals')->firstOrFail();

        $preview = $this->upload($request->preview);

        Tip::create([
            'preview' => $preview,
            'title' => $request->title,
            'address' => $request->address,
            'google' => $request->google,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'weblink' => $request->weblink,
            'topic_id' => $topic->id,
            'city_id' => $request->city_id,
            'category_id' => $request->category_id,
            'author_id' => $request->author_id
        ]);

        return redirect()->route('tip.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tip  $tip
     * @return \Illuminate\Http\Response
     */
    public function edit(Tip $tip)
    {
        $authors = Author::latest()->get();
        $cities = City::all();
        $categories = Topic::where('name_en', 'Tips From Locals')->firstOrFail()->categories()->get();

        return view('admin.tip.edit', compact(['authors', 'categories', 'cities', 'tip']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tip  $tip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tip $tip)
    {
        $request->validate([
            'author_id' => 'required|exists:authors,id',
            'category_id' => 'required|exists:categories,id',
            'city_id' => 'required|exists:cities,id',
            'title' => 'required',
            'address' => 'required',
            'google' => 'required|check_coords',
            'description_en' => 'required',
            'description_de' => 'required',
            'weblink' => 'required|url',
        ]);

        $preview = $tip->preview;

        if ($request->preview) {
            $request->validate([
                'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=512,min_height=800',
            ]);

            $preview = $this->upload($request->preview, true, $preview);
        }

        $tip->update([
            'preview' => $preview,
            'title' => $request->title,
            'address' => $request->address,
            'google' => $request->google,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'weblink' => $request->weblink,
            'city_id' => $request->city_id,
            'category_id' => $request->category_id,
            'author_id' => $request->author_id
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tip  $tip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tip $tip)
    {
        $tip->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('tip.index');
    }

    /**
     * Set disabled status.
     *
     * @param Tip $tip
     */
    public function diactivate(Tip $tip)
    {
        $tip->status = !$tip->status;
        $tip->save();

        return back();
    }
}
