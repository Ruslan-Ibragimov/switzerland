<?php

namespace App\Http\Controllers\API;

use App\Filters\EntryFilter;
use App\Http\Controllers\Controller;
use App\Services\Api\TopicService;
use App\Topic;

class TopicController extends Controller
{
    protected $topicservice;
    public function __construct(TopicService $topicservice)
    {
        $this->topicservice = $topicservice;
    }

    public function index()
    {
        $this->topicservice->index();
    }

    public function show(Topic $topic, EntryFilter $filter)
    {
        $this->topicservice->show($topic, $filter);
    }
}
