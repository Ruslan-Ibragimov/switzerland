<?php

namespace App\Http\Controllers\API;

use App\City;
use App\Http\Controllers\Controller;
use App\Services\Api\GeneralService;
use Illuminate\Http\Request;

class CityController extends Controller
{
    protected $generalservice;

    public function __construct(GeneralService $generalservice)
    {
        $this->generalservice = $generalservice;
    }
    public function index()
    {
        $this->generalservice->cities();
    }
}
