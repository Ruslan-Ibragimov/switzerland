<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AuthRequest;
use App\Services\Api\AuthService;

class RegisterController extends Controller
{
    protected $authservice;

    public function __construct(AuthService $authservice)
    {
        $this->authservice = $authservice;
    }
    public function register(AuthRequest $request)
    {
        $request->validated();
        $this->authservice->register($request);
    }
    public function forget(AuthRequest $request)
    {
        $request->validated();
        $this->authservice->forget($request);
    }
}
