<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AuthRequest;
use App\Services\Api\AuthService;

class LoginController extends Controller
{
    protected $authservice;

    public function __construct(AuthService $authservice)
    {
        $this->authservice = $authservice;
    }

    public function login(AuthRequest $request)
    {
        $request->validated();
        $this->authservice->login($request);
    }
    public function logout()
    {
        $this->authservice->logout();
    }
}
