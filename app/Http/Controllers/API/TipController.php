<?php

namespace App\Http\Controllers\API;

use App\Author;
use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\EntryRequest;
use App\Services\Api\EntryService;
use App\Services\Api\GeneralService;
use App\Tip;
use Illuminate\Http\Request;

class TipController extends Controller
{
    use EntryFilterControllerExtend;

    protected $entryservice;
    protected $generalservice;
    public function __construct(EntryService $entryservice, GeneralService $generalservice)
    {
        $this->middleware('auth:sanctum')->only('store');
        $this->entryservice = $entryservice;
        $this->generalservice = $generalservice;
    }

    public function main(EntryFilter $filters)
    {
        $this->generalservice->tipsResource($filters);
    }


    public function author(Author $author, EntryFilter $filters)
    {
        $this->generalservice->authorTips($author, $filters);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {

        $this->entryservice->index($filters, Tip::class);
    }

    /**
     * Add enty in bucket.
     *
     * @param Request $request
     */
    public function store(EntryRequest $request)
    {
        $this->entryservice->store($request, Tip::class);
    }

    /**
     * Display the specified resource.
     *
     * @param  Tip $tip
     * @return \Illuminate\Http\Response
     */
    public function show(Tip $tip)
    {
        $this->entryservice->show($tip);
    }
}
