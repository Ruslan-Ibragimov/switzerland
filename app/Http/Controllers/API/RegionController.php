<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\Api\GeneralService;

class RegionController extends Controller
{
    protected $generalservice;

    public function __construct(GeneralService $generalservice)
    {
        $this->generalservice = $generalservice;
    }
    public function index()
    {
        $this->generalservice->regions();
    }
}
