<?php

namespace App\Http\Controllers\API;

use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\EntryRequest;
use App\Services\Api\EntryService;
use App\Services\Api\GeneralService;
use App\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    use EntryFilterControllerExtend;


    protected $entryservice;
    protected $generalservice;

    public function __construct(EntryService $entryservice, GeneralService $generalservice)
    {
        $this->middleware('auth:sanctum')->only('store');
        $this->entryservice = $entryservice;
        $this->generalservice = $generalservice;
    }

    public function main(EntryFilter $filters)
    {
        $this->generalservice->shopsResource($filters);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $this->entryservice->index($filters, Shop::class);
    }

    /**
     * Add enty in bucket.
     *
     * @param Request $request
     */
    public function store(EntryRequest $request)
    {
        $this->entryservice->store($request, Shop::class);
    }

    /**
     * Display the specified resourc\e.
     *
     * @param  Shop $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        $this->entryservice->show($shop);
    }
}
