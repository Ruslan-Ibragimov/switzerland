<?php

namespace App\Http\Controllers\API;

use App\Advertising;
use App\Bucket;
use App\Filters\EntryFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\EntryRequest;
use App\Services\Api\BucketService;
use Illuminate\Http\Request;

class BucketController extends Controller
{
    protected $bucketservice;
    public function __construct(BucketService $bucketservice)
    {
        $this->bucketservice = $bucketservice;
    }
    public function index(EntryFilter $filter)
    {
        $this->bucketservice->index($filter);
    }
    public function countWeblink(Advertising $advertising)
    {
        $this->bucketservice->countWeblink($advertising);
    }
    public function destroy(Bucket $bucket)
    {
        $this->bucketservice->destroy($bucket);
    }

    public function exportBucket(EntryRequest $request)
    {
        $this->bucketservice->exportBucket($request);
    }
}
