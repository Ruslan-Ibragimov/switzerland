<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SupportRequest;
use App\Services\Api\GeneralService;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    protected $service;
    public function __construct(GeneralService $service)
    {
        $this->service = $service;
    }

    public function userRequest(SupportRequest $request)
    {
        $this->service->sendUserRequest($request->all());
    }
}
