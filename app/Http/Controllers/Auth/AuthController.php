<?php

namespace App\Http\Controllers\Auth;

use App\EmailVerify;
use App\Http\Controllers\Controller;
use App\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{


    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
    public function resetPassword(Request $request)
    {
        $token = $request->token;
        $email = $request->email;
        $hide_nav = true;
        return view('auth.passwords.reset', compact(['email', 'token', 'hide_nav']));
    }
    public function updatePassword(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|check_email',
            'password' => 'required|string|min:8|confirmed',
        ]);
        $password_reset = PasswordReset::where('token', $request->token)->where('email', $request->email)->first();
        $msg = 'Reset password  link is not valid';
        if (!isset($password_reset->email) || (strtotime($password_reset->created_at) + 60 * 60) < strtotime(date('Y-m-d H:i:s'))) {
            return   redirect('/welcome')->with(['msg' => $msg]);
        }
        $user = User::where('email', $request->email)->first();
        $user->update([
            'password' => Hash::make($request->password)
        ]);
        $msg = 'Your password has been successfully changed !';
        return redirect('/welcome')->with(['msg' => $msg]);
    }
    public function accountActivate(Request $request)
    {
        $email_verfify = EmailVerify::where('token', $request->token)->where('email', $request->email)->first();
        $msg = 'Account activate link is not valid';
        if (!isset($email_verfify->email)) {
            redirect('/welcome')->with(['msg' => $msg]);
        };
        $user = User::where('email', $request->email)->first();
        $user->update(['status' => 1]);
        $msg = 'Your account has been successfully activated !';
        return redirect('/welcome')->with(['msg' => $msg]);
    }
}
