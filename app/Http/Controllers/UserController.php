<?php

namespace App\Http\Controllers;

use App\BucketImport;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Filters\UserFilter;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserFilter $filters)
    {
        $users = User::latest()->filter($filters)->with('bucket')->paginate(25);

        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.user.edit', compact('user'));
    }

    public function store(Request $request)
    {
        $user = $request->validate([
            'last_name' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8'
        ]);

        User::create($user);

        return redirect()->route('user.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'last_name' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255'
        ]);

        if ($request->last_name !== $user->last_name) {
            $user->last_name = $request->last_name;
        }

        if ($request->name !== $user->name) {
            $user->name = $request->name;
        }

        if ($request->email !== $user->email) {
            $request->validate([
                'email' => 'unique:users',
            ]);

            $user->email = $request->email;
        }

        if ($request->password) {
            $request->validate([
                'password' => 'required|string|min:8'
            ]);

            $user->password = Hash::make($request->password);
        }

        $user->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $user->tokens()->delete();
        $user->delete();

        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('user.index');
    }

    /**
     * Export all users to exel
     *
     * @return mixed
     */
    public function export()
    {
        return Excel::download(new UsersExport(), 'users(' . date("Y-m-d H-i-s") . ').xlsx');
    }


    public function bucketMap($token)
    {
        $data = BucketImport::findOrFail($token);
        $data->geo_info = json_decode($data->geo_info);
        return view('bucket.index')->with('data', $data);
    }
}
