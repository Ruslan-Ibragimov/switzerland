<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

trait ImageUploader
{
  public $uploadFolder = 'uploads/';

  public function upload($image, bool $update = false, string $oldImagePath = '')
  {

    if ($update) {
      File::delete($oldImagePath);
    }

    $imageName = time() + rand() . '.' . $image->getClientOriginalExtension();

    $path = $image->move(public_path($this->uploadFolder), $imageName);

    return $this->uploadFolder . $imageName;
  }
}
