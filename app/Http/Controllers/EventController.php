<?php

namespace App\Http\Controllers;

use App\Event;
use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Topic;
use Illuminate\Http\Request;

class EventController extends Controller
{
    use ImageUploader, EntryFilterControllerExtend;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $events = $this->filter(Event::class, $filters)->paginate(25);
        $categories = Topic::where('name_en', 'Events & Tickets')->firstOrFail()->categories()->get();

        return view('admin.event.index', compact(['events', 'categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Topic::where('name_en', 'Events & Tickets')->firstOrFail()->categories()->get();

        return view('admin.event.create', compact(['categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=1120,min_height=570',
            'category_id' => 'required|exists:categories,id',
            'title' => 'required',
            'address' => 'required',
            'google' => 'required|check_coords',
            'description_en' => 'required',
            'description_de' => 'required',
            'valid_from' => 'required|date',
            'valid_to' => 'required|date'
        ]);
        if ($request->video_link) {
            $request->validate([
                'video_link' => 'url',
            ]);
        }
        // Get topic
        $topic = Topic::where('name_en', 'Events & Tickets')->firstOrFail();

        // Uplaod preview image
        $preview = $this->upload($request->preview);

        // Create new entry
        $event = Event::create([
            'video_link' => $request->video_link,
            'preview' => $preview,
            'title' => $request->title,
            'address' => $request->address,
            'google' => $request->google,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'tickets' => $request->tickets,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'app_tipp' => $request->app_tipp,
            'app_tipp_android' => $request->app_tipp_android,
            'phone' => $request->phone,
            'topic_id' => $topic->id,
            'category_id' => $request->category_id,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,
        ]);

        return redirect()->route('event.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $categories = Topic::where('name_en', 'Events & Tickets')->firstOrFail()->categories()->get();

        return view('admin.event.edit', compact(['categories', 'event']));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'title' => 'required',
            'address' => 'required',
            'google' => 'required|check_coords',
            'description_en' => 'required',
            'description_de' => 'required',
            'valid_from' => 'required|date',
            'valid_to' => 'required|date'
        ]);
        if ($request->video_link) {
            $request->validate([
                'video_link' => 'url',
            ]);
        }
        $preview = $event->preview;

        if ($request->preview) {
            $request->validate([
                'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=1120,min_height=570',
            ]);

            $preview = $this->upload($request->preview, true, $preview);
        }

        $event->update([
            'video_link' => $request->video_link,
            'preview' => $preview,
            'title' => $request->title,
            'address' => $request->address,
            'google' => $request->google,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'tickets' => $request->tickets,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'app_tipp' => $request->app_tipp,
            'app_tipp_android' => $request->app_tipp_android,
            'phone' => $request->phone,
            'category_id' => $request->category_id,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Event $event)
    {
        $event->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('event.index');
    }

    /**
     * Set disabled status.
     *
     * @param Event $event
     */
    public function diactivate(Event $event)
    {
        $event->status = !$event->status;
        $event->save();

        return back();
    }
}
