<?php

namespace App\Http\Controllers;

use App\Advertising;
use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class AdvertisingController extends Controller
{
    use ImageUploader, EntryFilterControllerExtend;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $ads = $this->filter(Advertising::class, $filters)->paginate(25);
        $categories = Topic::all();

        return view('admin.ad.index', compact(['ads', 'categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Topic::all();

        return view('admin.ad.create', compact(['categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'preview' => 'required|mimes:jpg,jpeg,png|dimensions:min_width=1120,min_height=780',
            'firma' => 'required',
            'weblink' => 'required|url',
            'topic_id' => 'required|exists:topics,id',
            'valid_from' => 'required',
            'valid_to' => 'required'
        ]);

        $preview = $this->upload($request->preview);

        $ad = Advertising::create([
            'preview' => $preview,
            'firma' => $request->firma,
            'weblink' => $request->weblink,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,
            'topic_id' => $request->topic_id
        ]);

        return redirect()->route('ad.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Advertising $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertising $ad)
    {
        $categories = Topic::all();

        return view('admin.ad.edit', compact(['categories', 'ad']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Advertising $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertising $ad)
    {
        $request->validate([
            'firma' => 'required',
            'weblink' => 'required|url',
            'topic_id' => 'required|exists:topics,id',
            'valid_from' => 'required',
            'valid_to' => 'required',
        ]);

        $preview = $ad->preview;

        if ($request->preview) {
            $request->validate([
                'preview' => 'required|mimes:jpg,jpeg,png|dimensions:min_width=1120,min_height=780',
            ]);

            $preview = $this->upload($request->preview, true, $preview);
        }

        $ad->update([
            'preview' => $preview,
            'firma' => $request->firma,
            'weblink' => $request->weblink,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,
            'topic_id' => $request->topic_id
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Advertising $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Advertising $ad)
    {
        $ad->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('ad.index');
    }

    /**
     * Set disabled status.
     *
     * @param Advertising $advertising
     */
    public function diactivate(Advertising $ad)
    {
        $ad->status = !$ad->status;
        $ad->save();

        return back();
    }
}
