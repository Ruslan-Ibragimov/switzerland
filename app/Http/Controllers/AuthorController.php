<?php

namespace App\Http\Controllers;

use App\Author;
use App\Filters\EntryFilter;
use App\Filters\UserFilter;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    use ImageUploader;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $authors = Author::latest()->filter($filters)->paginate(25);

        return view('admin.author.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|mimes:jpg,jpeg|dimensions:min_width=512,min_height=512',
            'name' => 'required',
            'proffession_en' => 'required',
            'proffession_de' => 'required',
        ]);

        $photo = $this->upload($request->photo);

        Author::create([
            'photo' => $photo,
            'name' => $request->name,
            'proffession_en' => $request->proffession_en,
            'proffession_de' => $request->proffession_de
        ]);

        return redirect()->route('author.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Author $author)
    {
        return view('admin.author.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        $request->validate([
            'name' => 'required',
            'proffession_en' => 'required',
            'proffession_de' => 'required',
        ]);

        $photo = $author->photo;

        if ($request->photo) {
            $request->validate([
                'photo' => 'required|mimes:jpg,jpeg|dimensions:min_width=512,min_height=512',
            ]);

            $photo = $this->upload($request->photo, true, $photo);
        }

        $author->update([
            'photo' => $photo,
            'name' => $request->name,
            'proffession_en' => $request->proffession_en,
            'proffession_de' => $request->proffession_de,
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Author $author)
    {
        $author->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);
        return redirect()->route('author.index');
    }
}
