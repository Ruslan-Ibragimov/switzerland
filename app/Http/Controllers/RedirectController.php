<?php

namespace App\Http\Controllers;

use App\LinkRedirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class RedirectController extends Controller
{
    public function index($token)
    {
        $data = LinkRedirect::where('token', $token)->first();
        if (!$data) abort(404);
        $model = $data->model_type;
        $item = $model::find($data->model_id);
        if ($item) {
            $item->recordActivity('weblink');
        }
        return redirect($data->link);
    }
}
