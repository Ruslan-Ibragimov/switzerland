<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Get all regions.
     *
     */
    public function index()
    {
        return Region::all();
    }
}
