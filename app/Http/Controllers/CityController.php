<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Get all cities.
     *
     * @return City[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return City::all();
    }
}
