<?php

namespace App\Http\Controllers;

use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Shop;
use App\Topic;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    use ImageUploader, EntryFilterControllerExtend;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $shops = $this->filter(Shop::class, $filters)->paginate(25);
        $categories = Topic::where('name_en', 'Souvenir Shops')->firstOrFail()->categories()->get();

        return view('admin.shop.index', compact(['shops', 'categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Topic::where('name_en', 'Souvenir Shops')->firstOrFail()->categories()->get();

        return view('admin.shop.create', compact(['categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=1120,min_height=750',
            'firma' => 'required',
            'weblink_title' => 'required',
            'weblink' => 'required|url',
            'category_id' => 'required|exists:categories,id',
            'description_en' => 'required',
            'description_de' => 'required',
            'valid_from' => 'required',
            'valid_to' => 'required'
        ]);

        // Get topic
        $topic = Topic::where('name_en', 'Souvenir Shops')->firstOrFail();

        // Uplaod preview image
        $preview = $this->upload($request->preview);

        // Create new entry
        $shop = Shop::create([
            'preview' => $preview,
            'firma' => $request->firma,
            'weblink_title' => $request->weblink_title,
            'weblink' => $request->weblink,
            'category_id' => $request->category_id,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,
            'topic_id' => $topic->id,
            'category_id' => $request->category_id
        ]);

        return redirect()->route('shop.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Shop $shop)
    {
        $categories = Topic::where('name_en', 'Souvenir Shops')->firstOrFail()->categories()->get();

        return view('admin.shop.edit', compact(['categories', 'shop']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop $shop)
    {
        $request->validate([
            'firma' => 'required',
            'weblink_title' => 'required',
            'weblink' => 'required|url',
            'category_id' => 'required|exists:categories,id',
            'description_en' => 'required',
            'description_de' => 'required',
            'valid_from' => 'required',
            'valid_to' => 'required'
        ]);

        $preview = $shop->preview;

        if ($request->preview) {
            $request->validate([
                'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=1120,min_height=750',
            ]);

            $preview = $this->upload($request->preview, true, $preview);
        }

        $shop->update([
            'preview' => $preview,
            'firma' => $request->firma,
            'weblink_title' => $request->weblink_title,
            'weblink' => $request->weblink,
            'category_id' => $request->category_id,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,
            'category_id' => $request->category_id
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Shop $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Shop $shop)
    {
        $shop->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('shop.index');
    }

    /**
     * Set disabled status.
     *
     * @param Shop $shop
     */
    public function diactivate(Shop $shop)
    {
        $shop->status = !$shop->status;
        $shop->save();

        return back();
    }
}
