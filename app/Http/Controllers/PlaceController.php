<?php

namespace App\Http\Controllers;

use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\Place;
use App\Region;
use App\Topic;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    use ImageUploader, EntryFilterControllerExtend;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $places = $this->filter(Place::class, $filters)->paginate(25);
        $categories = Topic::where('name_en', 'Must See')->firstOrFail()->categories()->get();
        $regions = Region::all();


        return view('admin.place.index', compact(['places', 'categories', 'regions']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::all();
        $categories = Topic::where('name_en', 'Must See')->firstOrFail()->categories()->get();

        return view('admin.place.create', compact(['regions', 'categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=1120,min_height=570',
            'region_id' => 'required|exists:regions,id',
            'category_id' => 'required|exists:categories,id',
            'title' => 'required',
            'address' => 'required',
            'google' => 'required|check_coords',
            'description_en' => 'required',
            'description_de' => 'required',
        ]);
        if ($request->video_link) {
            $request->validate([
                'video_link' => 'url',
            ]);
        }
        // Get topic
        $topic = Topic::where('name_en', 'Must See')->firstOrFail();

        // Uplaod preview image
        $preview = $this->upload($request->preview);

        // Create new entry
        $place = Place::create([
            'video_link' => $request->video_link,
            'preview' => $preview,
            'title' => $request->title,
            'address' => $request->address,
            'google' => $request->google,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'weblink' => $request->weblink,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'app_tipp' => $request->app_tipp,
            'app_tipp_android' => $request->app_tipp_android,
            'phone' => $request->phone,
            'region_id' => $request->region_id,
            'topic_id' => $topic->id,
            'category_id' => $request->category_id
        ]);

        return redirect()->route('place.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Place $place)
    {
        $regions = Region::all();
        $categories = Topic::where('name_en', 'Must See')->firstOrFail()->categories()->get();
        return view('admin.place.edit', compact(['regions', 'categories', 'place']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $place)
    {
        $request->validate([
            'region_id' => 'required|exists:regions,id',
            'category_id' => 'required|exists:categories,id',
            'title' => 'required',
            'address' => 'required',
            'google' => 'required|check_coords',
            'description_en' => 'required',
            'description_de' => 'required',
        ]);
        $preview = $place->preview;
        if ($request->video_link) {
            $request->validate([
                'video_link' => 'url',
            ]);
        }
        if ($request->preview) {
            $request->validate([
                'preview' => 'required|mimes:jpg,jpeg|dimensions:min_width=1120,min_height=570',
            ]);

            $preview = $this->upload($request->preview, true, $preview);
        }

        $place->update([
            'video_link' => $request->video_link,
            'preview' => $preview,
            'title' => $request->title,
            'address' => $request->address,
            'google' => $request->google,
            'description_en' => $request->description_en,
            'description_de' => $request->description_de,
            'weblink' => $request->weblink,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'app_tipp' => $request->app_tipp,
            'app_tipp_android' => $request->app_tipp_android,
            'phone' => $request->phone,
            'region_id' => $request->region_id,
            'category_id' => $request->category_id
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Place $place
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Place $place)
    {
        $place->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('place.index');
    }

    /**
     * Set disabled status.
     *
     * @param Event $event
     */
    public function diactivate(Place $place)
    {
        $place->status = !$place->status;
        $place->save();

        return back();
    }
}
