<?php

namespace App\Http\Controllers;

use App\EmailBanner;
use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;
use App\LinkRedirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class EmailBannerController extends Controller
{
    use ImageUploader, EntryFilterControllerExtend;

    protected $types = [
        'Account Activation',
        'Bucket List',
    ];

    protected $langs = [
        'English',
        'Deutsch'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EntryFilter $filters)
    {
        $banners = $this->filter(EmailBanner::class, $filters)->paginate(25);
        return view('admin.email_banners.index')
            ->with('banners', $banners)
            ->with('types', $this->types)
            ->with('langs', $this->langs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.email_banners.create')
            ->with('types', $this->types)
            ->with('langs', $this->langs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'img' => 'required|mimes:jpg,jpeg|dimensions:min_width=832,min_height=276',
            'type' => 'required',
            'lang' => 'required',
            'title' => 'required',
            'weblink' => 'required|url',
            'valid_from' => 'required|date',
            'valid_to' => 'required|date'
        ]);


        // Uplaod img image
        $img = $this->upload($request->img);

        // Create new entry
        $emailBanner = EmailBanner::create([
            'img' => $img,
            'title' => $request->title,
            'type' => $request->type,
            'lang' => $request->lang,
            'title' => $request->title,
            'weblink' => $request->weblink,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,
        ]);

        $emailBanner->redirect =   $this->createOrUpdateRedirect(EmailBanner::class, $emailBanner->id, $request->weblink);
        $emailBanner->save();
        return redirect()->route('email-banner.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailBanner $emailBanner)
    {

        return view('admin.email_banners.edit')
            ->with('banner', $emailBanner)
            ->with('types', $this->types)
            ->with('langs', $this->langs);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailBanner $emailBanner)
    {
        $request->validate([
            'type' => 'required',
            'lang' => 'required',
            'title' => 'required',
            'weblink' => 'required|url',
            'valid_from' => 'required|date',
            'valid_to' => 'required|date'
        ]);

        $img = $emailBanner->img;

        if ($request->img) {
            $request->validate([
                'img' => 'required|mimes:jpg,jpeg|dimensions:min_width=832,min_height=276',

            ]);

            $img = $this->upload($request->img, true, $img);
        }

        $emailBanner->update([
            'img' => $img,
            'title' => $request->title,
            'type' => $request->type,
            'lang' => $request->lang,
            'title' => $request->title,
            'weblink' => $request->weblink,
            'valid_from' => $request->valid_from,
            'valid_to' => $request->valid_to,

        ]);



        $emailBanner->redirect =   $this->createOrUpdateRedirect(EmailBanner::class, $emailBanner->id, $request->weblink, $emailBanner->redirect);
        $emailBanner->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param EmailBanner $emailBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmailBanner $emailBanner)
    {

        $emailBanner->delete();
        if ($request->ajax()) return response()->json(['success' => true], 200);

        return redirect()->route('email-banner.index');
    }

    protected function createOrUpdateRedirect($model, $id, $link, $oldToken = null)
    {
        $token = Str::random(60);
        $token = strtotime(date("Y-m-d H:i:s")) . $token;
        if ($oldToken) {
            $result = LinkRedirect::where('token', $oldToken)->first();
        }

        if (!isset($result) || !isset($token)) {
            $result = new LinkRedirect();
        }
        $result->token =  $token;
        $result->model_type =  $model;
        $result->model_id =  $id;
        $result->link =  $link;
        $result->created_at =  date('Y-m-d H:i:s');
        $result->save();
        return $token;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param EmailBanner $emailBanner
     */
    public function diactivate(EmailBanner $emailBanner)
    {
        $emailBanner->status = !$emailBanner->status;
        $emailBanner->save();

        return back();
    }
}
