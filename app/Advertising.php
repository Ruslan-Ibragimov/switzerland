<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
    use EntryFilter, RecordsActivity;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['topic'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'preview',
        'firma',
        'weblink',
        'valid_from',
        'valid_to',
        'topic_id'
    ];

    /**
     * Get Topic.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
