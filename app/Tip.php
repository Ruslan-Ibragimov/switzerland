<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    use Categoriable, RecordsBucket, EntryFilter, RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'preview',
        'title',
        'address',
        'google',
        'description_en',
        'description_de',
        'weblink',
        'topic_id',
        'city_id',
        'category_id',
        'author_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['categories', 'city'];

    /**
     * Get city of the tips from local.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Get topic of the entry.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    /**
     * Get author of the tip.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
