<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $token;
    protected $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $email)
    {
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $user = User::where('email', $this->email)->first();
        $url = route('reset.password') . "?token=$this->token&email=$this->email";
        return $this->from(env('MAIL_FROM_ADDRESS'),  'Grüezi Switzerland')
            ->subject(__('general.reset_password'))
            ->view('emails.forgot_password.index')->with('url', $url)->with('user', $user)
            ->withSwiftMessage(function ($message) {
                $message->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
            });
    }
}
