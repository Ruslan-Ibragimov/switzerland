<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountVerifyMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $token;
    protected $email;
    protected $user;
    protected $banner;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $email, $user, $banner)
    {
        $this->token = $token;
        $this->email = $email;
        $this->user = $user;
        $this->banner = $banner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $url = route('activate.account') . "?token=$this->token&email=$this->email";


        return $this->from(env('MAIL_FROM_ADDRESS'), 'Grüezi Switzerland')
            ->subject(__('general.activation_subject'))
            ->view('emails.account.verify')
            ->with('url', $url)->with('user', $this->user)->with('banner', $this->banner)
            ->withSwiftMessage(function ($message) {
                $message->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
            });
    }
}
