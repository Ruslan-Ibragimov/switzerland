<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BucketExport extends Mailable
{
    use Queueable, SerializesModels;
    protected $token;
    protected $user;
    protected $banner;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $user, $banner)
    {
        $this->token = $token;
        $this->user = $user;
        $this->banner = $banner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $url = route('bucket.list', ['token' => $this->token]);

        return $this->from(env('MAIL_FROM_ADDRESS'), 'Grüezi Switzerland')
            ->subject(__('general.bucket_subject'))
            ->view('emails.bucket.index')
            ->with('url', $url)->with('user', $this->user)->with('banner', $this->banner)
            ->withSwiftMessage(function ($message) {
                $message->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
            });
    }
}
