<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupportMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from(env('MAIL_FROM_ADDRESS'),  'Grüezi Switzerland')
            ->subject('Grüezi Switzerland Support Request')
            ->view('emails.support.index')->with('data', $this->data)
            ->withSwiftMessage(function ($message) {
                $message->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
            });
    }
}
