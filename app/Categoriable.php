<?php

namespace App;

trait Categoriable
{
    /**
     * Get categories of entry
     *
     * @return string
     */
    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * Add or update categories for entry.
     *
     * @param  array $category
     * @return string
     */
    public function addCategories(array $categories)
    {
        return $this->categoty_id = $category;
    }
}
