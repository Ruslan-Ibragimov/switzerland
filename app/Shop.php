<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    use Categoriable, RecordsBucket, EntryFilter, RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'preview',
        'firma',
        'weblink_title',
        'weblink',
        'description_en',
        'description_de',
        'valid_from',
        'valid_to',
        'topic_id',
        'category_id'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['categories'];

    /**
     * Get topic of the entry.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
