<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkRedirect extends Model
{
    protected $table = 'redirects';
    protected $primaryKey = 'token';
}
