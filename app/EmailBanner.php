<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailBanner extends Model
{
    use  EntryFilter, RecordsActivity;

    protected $table = 'email_banner';
    protected $guarded = ['id'];
}
