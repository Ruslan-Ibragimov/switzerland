<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Categoriable, RecordsBucket, EntryFilter, RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_link',
        'preview',
        'title_en',
        'title_de',
        'description_en',
        'description_de',
        'weblink',
        'facebook',
        'instagram',
        'app_tipp',
        'phone',
        'topic_id',
        'category_id',
        'weblink_de',
        'app_tipp_de',
        'app_tipp_android_de',
        'app_tipp_android'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['categories'];

    /**
     * Get topic of the entry.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
