<?php

namespace App\Filters;

use App\Topic;

class EntryFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['topic', 'category', 'region', 'city', 'city_tips', 'category_tips', 'author', 'title', 'title_de', 'firma', 'ad_topic', 'vorname', 'type'];

    /**
     * Find entries by topic.
     *
     * @param $city
     */
    protected function topic($topic)
    {
        return $this->builder->whereHasMorph('entry', '*', function ($query) use ($topic) {
            $query->where('topic_id', $topic);
        });
    }

    /**
     * Find entries by category.
     *
     * @param $category
     */
    protected function category($category)
    {
        return $this->builder->where('category_id', $category);
    }

    /**
     * Find entries by region.
     *
     * @param $region
     */
    protected function region($region)
    {
        return $this->builder->where('region_id', $region);
    }

    /**
     * Find entries by city.
     *
     * @param $city
     */
    protected function city($city)
    {
        return $this->builder->where('city_id', $city);
    }
    /**
     * Find entries by city.
     *
     * @param $city
     */
    protected function city_tips($city)
    {
        return $this->builder->whereHas('tips', function ($query) use ($city) {
            $query->where('city_id', $city);
        });
    }
    /**
     * Find entries by city.
     *
     * @param $city
     */
    protected function category_tips($category)
    {
        return $this->builder->whereHas('tips', function ($query) use ($category) {
            $query->where('category_id', $category);
        });
    }
    /**
     * Find entries by city.
     *
     * @param $city
     */
    protected function author($author)
    {
        return $this->builder->where('id', $author);
    }

    /**
     * Find entries by title.
     *
     * @param $title
     */
    protected function title($title)
    {
        return $this->builder->where('title', 'like', $title . '%');
    }

    /**
     * Find entries by title_de.
     *
     * @param $title
     */
    protected function title_de($title)
    {
        return $this->builder->where('title_de', 'like', $title . '%');
    }

    /**
     * Find entries by firma.
     *
     * @param $firma
     */
    protected function firma($firma)
    {
        return $this->builder->where('firma', 'like', $firma . '%');
    }

    /**
     * Find entries by ad topic.
     *
     * @param $topic
     */
    protected function ad_topic($topic)
    {
        return $this->builder->where('topic_id', $topic);
    }

    /**
     * Find entries by vorname.
     *
     * @param $vorname
     */
    protected function vorname($vorname)
    {
        return $this->builder->where('name', 'like', $vorname . '%');
    }
    /**
     * Find entries by vorname.
     *
     * @param $vorname
     */
    protected function type($type)
    {
        return $this->builder->where('type', 'like', $type . '%');
    }
}
