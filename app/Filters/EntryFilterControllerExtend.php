<?php


namespace App\Filters;


use App\Filters\EntryFilter;
use App\Place;

trait EntryFilterControllerExtend
{
    /**
     * Filter places.
     *
     * @param EntryFilter $filters
     * @return mixed
     */
    public function filter($entryModel, EntryFilter $filters)
    {
        return $entryModel::latest()->filter($filters);
    }
}
