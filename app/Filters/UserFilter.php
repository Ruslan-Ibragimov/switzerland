<?php

namespace App\Filters;

class UserFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['status', 'vorname'];

    /**
     * Find user by status.
     *
     * @param $status
     */
    protected function status($status)
    {
        return $this->builder->where('status', filter_var($status, FILTER_VALIDATE_BOOLEAN));
    }

    /**
     * Find entries by vorname.
     *
     * @param $vorname
     */
    protected function vorname($vorname)
    {
        return $this->builder->where('name', 'like', $vorname . '%');
    }
}
