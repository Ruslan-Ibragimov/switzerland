<?php


namespace App;


trait RecordsBucket
{
    protected static function bootRecordsBucket()
    {
        static::deleting(function ($model) {
            $model->entries()->delete();
        });
    }

    /**
     * Fetch the entries relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function entries()
    {
        return $this->morphMany('App\Bucket', 'entry');
    }

    /**
     * Record new entry for the model.
     *
     */
    public function recordBucket()
    {
        $attributes = ['user_id' => auth()->id()];
        $response = [
            'success' => true,
            'result' => (object) [],
            'error' => (object) [],
            'code' => 200
        ];
        if (!$this->entries()->where($attributes)->where('entry_type', $this->getEntryType())->exists()) {
            $response['result']->data = $this->entries()->create($attributes);
        } else {
            $response['success'] = false;
            $response['error'] = ['msg' => 'This entry is already in your bucket list'];
            $response['code'] = 400;
        }
        return $response;
    }

    /**
     * Determine the enrty type.
     *
     * @return string
     */
    protected function getEntryType()
    {
        return ucwords((new \ReflectionClass($this))->getName());
    }

    /**
     * Get the number of watchist for the entry.
     *
     * @return integer
     */
    public function scopeGetWatchListCount()
    {
        return $this->entries->count();
    }
}
