<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_en', 'name_de'];
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        $lang = app()->getLocale();
        static::addGlobalScope(function (Builder $query) use ($lang) {
            $orderBy = $lang === 'de' ? 'name_de' : 'name_en';
            $query->orderBy($orderBy);
        });
    }
}
