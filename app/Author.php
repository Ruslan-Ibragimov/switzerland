<?php

namespace App;

use App\Filters\UserFilter as Filters;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use  EntryFilter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo',
        'name',
        'proffession_en',
        'proffession_de',
    ];

    public function tips()
    {
        return $this->hasMany(Tip::class);
    }
}
