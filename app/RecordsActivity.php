<?php

namespace App;


trait RecordsActivity
{
    /**
     * Record new activity for the model.
     *
     * @param string $event
     */
    public function recordActivity($event)
    {
        $this->activity()->create([
            'type' => $event
        ]);
    }

    /**
     * Fetch the activity relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activity()
    {
        return $this->morphMany(Activity::class, 'subject');
    }


    /**
     * Fetch the activity relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function scopeGetActivityCount($event, $type)
    {
        return $this->activity()->where('type', $type)->count();
    }
}
