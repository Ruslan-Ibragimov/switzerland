<?php

namespace App\Services\Api;

use App\Topic;

class TopicService extends BaseService
{
    public function index()
    {
        $response = [
            'data' => Topic::latest()->get()
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function show($topic, $filter)
    {

        $response = [
            'topic' => $topic->load('categories'),
            'entries' => []
        ];
        if ($topic->id === 1) {
            $response['entries'] =  $topic->places()->filter($filter)->paginate(25);
        } elseif ($topic->id === 2) {
            $response['entries'] =  $topic->events()->filter($filter)->paginate(25);
        } elseif ($topic->id === 3) {
            $response['entries'] =  $topic->tips()->filter($filter)->paginate(25);
        } elseif ($topic->id === 4) {
            $response['entries'] =  $topic->services()->filter($filter)->paginate(25);
        } elseif ($topic->id === 5) {
            $response['entries'] = $topic->shops()->filter($filter)->paginate(25);
        }
        $this->sendResponse(true, $response, [], 200);
    }
}
