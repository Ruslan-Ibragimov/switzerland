<?php

namespace App\Services\Api;

use App\Bucket;
use App\BucketImport;
use App\EmailBanner;
use App\Mail\BucketExport;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class BucketService extends BaseService
{
    public function index($filter)
    {

        $bucket = Bucket::bucket(auth()->user())->filter($filter)->get();
        (object) $bucket =  collect($bucket)->mapToGroups(function ($item, $key) {
            return [$item['entry']['topic']['slug'] => $item];
        });
        $response = [
            'data' => count($bucket) != 0 ? $bucket : (object) []
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function countWeblink($model)
    {
        $model->recordActivity('weblink');
        $this->sendResponse(true, ['msg' => 'Successfully incremented'], [], 200);
    }
    public function destroy($bucket)
    {
        if ($bucket->user_id != auth()->user()->id)
            $this->sendResponse(false, [], ['msg' => __('auth.not_users_bucket')], 400);
        $bucket->delete();

        $this->sendResponse(true, ['msg' => __('auth.bucket_delete')], [], 200);
    }
    public function exportBucket($request)
    {

        $token = Str::random(60);
        $token = strtotime(date("Y-m-d H:i:s")) . $token;
        $data = [];
        $bucket = Bucket::bucket(auth()->user())->get();
        if (count($bucket) == 0)  $this->sendResponse(false, [], ['msg' => __('auth.empty_bucket')], 400);
        collect($bucket)->mapToGroups(function ($item, $key) use (&$data) {
            if ($this->checkType($item['entry']['topic']['slug'] && $item->entry->google)) {
                $coords = explode(',', $item->entry->google);
                $data[$item->entry->topic->slug][] = [
                    'id' => $item->entry->id,
                    'address' => $item->entry->address,
                    'title' => $item->entry->title,
                    'created_at' =>  $item->created_at->format("Y-m-d H:i"),
                    'lat' => $coords[0],
                    'lng' => $coords[1],
                ];
            }
            return [];
        });


        $lang = 'English';
        $type = 'Bucket List';
        if (app()->getLocale() == 'de') $lang = 'Deutsch';

        $banner = EmailBanner::where('valid_to', '>=', date('Y-m-d'))
            ->where('valid_from', '<=', date('Y-m-d'))
            ->where('lang', $lang)
            ->where('type', $type)
            ->where('status', 1)
            ->orderBy('valid_from', 'DESC')->first();


        BucketImport::insert([
            'email' => $request->email,
            'token' => $token,
            'geo_info' => json_encode($data),
            'user_id' => auth()->user()->id,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        Mail::to($request->email)->send(new BucketExport($token, auth()->user(), $banner));
        $response = [
            'msg' => __('auth.bucket_exported', ['email' => $request->email]),
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    protected function checkType($data)
    {
        return ($data == 'must-see' || $data == 'events-tickets'  || $data == 'local-tips') ? true : false;
    }
}
