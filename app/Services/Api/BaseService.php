<?php

namespace App\Services\Api;

use Illuminate\Http\Exceptions\HttpResponseException;

class BaseService
{

    protected function sendResponse($success, $result = [], $error = [], $status_code)
    {
        throw new HttpResponseException(
            response()->json([
                'success' => $success,
                'result' => (object) $result,
                'error' => (object)  $error
            ], $status_code)
        );
    }
}
