<?php

namespace App\Services\Api;

use App\Filters\EntryFilter;
use App\Filters\EntryFilterControllerExtend;

use App\Event;
use App\Place;
use App\Service;

class EntryService extends BaseService
{
    use EntryFilterControllerExtend;

    public function store($request, $model)
    {
        $response = $model::findOrFail($request->id)->recordBucket();
        $this->sendResponse($response['success'], $response['result'], $response['error'],  $response['code']);
    }

    public function index($filters, $model)
    {
        $response = [
            'data' => $this->filter($model, $filters)->where('status', 1)->paginate(25)
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function show($model)
    {
        $model->recordActivity('detail');
        $response = [
            'data' => $model
        ];
        $this->sendResponse(true, $response, [], 200);
    }
}
