<?php

namespace App\Services\Api;

use App\User;
use Illuminate\Support\Facades\Hash;

class UserService extends BaseService
{
    public function login($request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            $this->sendResponse(false, [], ['message' => __('auth.failed')], 403);
        }
        $token = $user->createToken('switzerland')->plainTextToken;
        $response = [
            'user' => $user,
            'token' => $token
        ];
        $this->sendResponse(true, $response, [], 201);
    }
    public function register($request)
    {
        $user = User::create($request->all());
        $token = $user->createToken($request->device_name)->plainTextToken;
        $response = [
            'user' => $user,
            'token' => $token
        ];
        $this->sendResponse(true, $response, [], 201);
    }
}
