<?php

namespace App\Services\Api;

use App\Author;
use App\City;
use App\Mail\SupportMail;
use App\Place;
use App\Region;
use App\Tip;
use App\Topic;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class GeneralService extends BaseService
{
    protected const MUST_SEE_ID = 1;
    protected const EVENTS_TICKETS_ID = 2;
    protected const LOCAL_TIPS_ID = 3;
    protected const SERVICES_ID = 4;
    protected const SOUVENIR_SHOPS = 5;

    public function cities()
    {
        $response = [
            'data' => City::all()
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function regions()
    {
        $response = [
            'data' => Region::all()
        ];
        $this->sendResponse(true, $response, [], 200);
    }


    public function placesResource($filter)
    {
        $request = $filter->getRequest();
        if (!$request->has('lat') || !$request->has('lng')) $this->sendResponse(false, [], ['msg' => __('validation.coords')], 400);

        $topic = Topic::with('categories', 'advertising')->find(self::MUST_SEE_ID);
        $this->adRecordActivity($topic->advertising);


        $entries =  $topic->places()->active()->filter($filter)
            ->orderBy(DB::raw(
                "6371 * acos (
                    cos ( radians($request->lat) )
                    * cos( radians( SUBSTRING_INDEX(google, ',' , 1) ) )
                    * cos( radians( SUBSTRING_INDEX(google, ',' , -1) ) - radians($request->lng) )
                    + sin ( radians($request->lat) )
                    * sin( radians( SUBSTRING_INDEX(google, ',' , 1) ) )
                    )
                "
            ), 'ASC')
            ->paginate(25);
        $response = [
            'topic' => $topic,
            'entries' => $entries,
        ];
        $this->sendResponse(true, $response, [], 200);
    }

    public function eventsResource($filter)
    {


        $request = $filter->getRequest();
        if (!$request->has('lat') || !$request->has('lng')) $this->sendResponse(false, [], ['msg' => __('validation.coords')], 400);
        $topic = Topic::with('categories', 'advertising')->find(self::EVENTS_TICKETS_ID);
        $this->adRecordActivity($topic->advertising);

        $entries = $topic->events()->active()->filter($filter)
            ->orderBy('valid_from')
            ->orderBy(DB::raw(
                "6371 * acos (
                    cos ( radians($request->lat) )
                    * cos( radians( SUBSTRING_INDEX(google, ',' , 1) ) )
                    * cos( radians( SUBSTRING_INDEX(google, ',' , -1) ) - radians($request->lng) )
                    + sin ( radians($request->lat) )
                    * sin( radians( SUBSTRING_INDEX(google, ',' , 1) ) )
                    )
                "
            ), 'ASC')
            ->paginate(25);
        foreach ($entries as $entry) {
            if ($entry->valid_from === $entry->valid_to) {
                $entry->event_date = utf8_encode(trim(strftime("%e.%b.%Y", strtotime($entry->valid_to)), " "));
            } else {
                $day = false;
                $month = false;
                $year = false;
                $result = "";

                $day1 = utf8_encode(trim(strftime("%e", strtotime($entry->valid_from)), " "));
                $day2 = utf8_encode(trim(strftime("%e", strtotime($entry->valid_to)), " "));
                if ($day1 === $day2) $day = true;

                $month1 = utf8_encode(trim(strftime("%b", strtotime($entry->valid_from)), " "));
                $month2 = utf8_encode(trim(strftime("%b", strtotime($entry->valid_to)), " "));
                if ($month1 === $month2) $month = true;

                $year1 = utf8_encode(trim(strftime("%Y", strtotime($entry->valid_from)), " "));
                $year2 = utf8_encode(trim(strftime("%Y", strtotime($entry->valid_to)), " "));
                if ($year1 === $year2) $year = true;
                if (!$day && !$month && !$year) {
                    $entry->event_date =  utf8_encode(strftime("%e.%b.%Y", strtotime($entry->valid_from))) . ' - ' . utf8_encode(strftime("%e.%b.%Y", strtotime($entry->valid_to)));
                } else {
                    if ($month && $year) {
                        $result =  $day1 . '-' . $day2 . '.' . $month1 . '.' . $year1;
                    } else {
                        $result = $day1 . '.' . $month1 . '.' . $year1 . '-' . $day2 . '.' . $month2 . '.' . $year2;
                    }


                    // if (!$day && !$month || (!$day && !$year) || (!$month && !$year)) {
                    // } else {
                    //     if ($day) $result = $day1;
                    //     else $result = $day1 . '-' . $day2;

                    //     if ($month) $result .=  '.' . $month1;
                    //     else $result .= '.' . $month1 . '-' . $month2;

                    //     if ($year) $result .=  '.' . $year1;
                    //     else $result .= '.' . $year1 . '-' . $year2;
                    // }


                    $entry->event_date = $result;
                }
            }
        }
        $response = [
            'topic' => $topic,
            'entries' => $entries,
        ];
        $this->sendResponse(true, $response, [], 200);
    }

    public function tipsResource($filter)
    {
        $topic = Topic::with('categories', 'advertising')->find(self::LOCAL_TIPS_ID);
        $this->adRecordActivity($topic->advertising);

        $request = $filter->getRequest();
        $authors = (object) [];
        if (isset($request->city) && isset($request->category)) {
            $authors = Author::whereHas('tips', function ($query) use ($request) {
                return $query->where('city_id', $request->city)->where('category_id', $request->category)->active();
            })->with(['tips' => function ($query) use ($request) {
                return $query->where('city_id', $request->city)->where('category_id', $request->category)->active();
            }])->paginate(25);
        }
        $response = [
            'topic' => $topic,
            'authors' => $authors,
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function authorTips($model, $filter)
    {
        $tips = Tip::where('author_id', $model->id)->filter($filter)->paginate(25);
        $response = [
            'author' => $model,
            'tips' => $tips
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function serviceResource($filter)
    {
        $topic = Topic::with('categories', 'advertising')->find(self::SERVICES_ID);
        $this->adRecordActivity($topic->advertising);

        $entries =  $topic->services()->active()->paginate(25);

        $response = [
            'topic' => $topic,
            'services' => $entries,
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function shopsResource($filter)
    {
        $topic = Topic::with('categories', 'advertising')->find(self::SOUVENIR_SHOPS);
        $this->adRecordActivity($topic->advertising);

        $entries =  $topic->shops()->active()->filter($filter)->paginate(25);
        $response = [
            'topic' => $topic,
            'shops' => $entries,
        ];
        $this->sendResponse(true, $response, [], 200);
    }

    public function sendUserRequest($request)
    {
        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new SupportMail($request));
        $response = [
            'msg' => __('auth.succes_request'),
        ];
        $this->sendResponse(true, $response, [], 200);
    }
    public function adRecordActivity($ads)
    {
        if (count($ads) != 0) {
            foreach ($ads as $ad) {
                $ad->recordActivity('detail');
            }
        } else return false;
    }
}
