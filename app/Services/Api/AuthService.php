<?php

namespace App\Services\Api;

use App\EmailBanner;
use App\EmailVerify;
use App\Mail\AccountVerifyMail;
use App\Mail\ForgotPasswordMail;
use App\PasswordReset;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthService extends BaseService
{
    public function login($request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            $this->sendResponse(false, [], ['msg' => __('auth.failed')], 403);
        }
        if (!$user->status) $this->sendResponse(false, [], ['msg' => __('auth.not_active')], 403);
        $token = $user->createToken('switzerland')->plainTextToken;
        $response = [
            'user' => $user,
            'token' => $token
        ];
        $this->sendResponse(true, $response, [], 201);
    }
    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();
        $this->sendResponse(true, ['msg' => __('auth.logout')], [], 201);
    }
    public function register($request)
    {
        $user = User::where('email', $request->email)->first();
        if (!isset($user->id)) $user = new User();
        $user->last_name = $request->last_name;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->status = 0;
        //Create Email Verify Token
        $email_token = Str::random(60);
        $response = $this->sendAccountVerify($request->email, $email_token, $user);
        if ($response) {
            $user->save();
            $token = $user->createToken($request->device_name)->plainTextToken;
            EmailVerify::insert([
                'email' => $request->email,
                'token' => $email_token,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $this->sendResponse(true, $response, [], 200);
        } else {
            $this->sendResponse(false, [], ['msg' => __('auth.email_failure')], 500);
        }
    }
    public function forget($request)
    {
        $token = Str::random(60);
        //Create Password Reset Token
        PasswordReset::insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        $this->sendResetEmail($request->email, $token);
    }
    private function sendResetEmail($email, $token)
    {
        Mail::to($email)->send(new ForgotPasswordMail($token, $email));
        $response = [
            'msg' => __('auth.reset_pass', ['email' => $email]),
        ];

        $this->sendResponse(true, $response, [], 200);
    }
    private function sendAccountVerify($email, $token, $user)
    {
        $lang = 'English';
        $type = 'Account Activation';
        if (app()->getLocale() == 'de') $lang = 'Deutsch';

        $banner = EmailBanner::where('valid_to', '>=', date('Y-m-d'))
            ->where('valid_from', '<=', date('Y-m-d'))
            ->where('lang', $lang)
            ->where('type', $type)
            ->where('status', 1)
            ->orderBy('valid_from', 'DESC')->first();

        Mail::to($email)->send(new AccountVerifyMail($token, $email, $user, $banner));

        $response = [
            'msg' => __('auth.activate_link', ['email' => $email]),
        ];
        return $response;
    }
}
