<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BucketImport extends Model
{
    protected $table = 'bucket_import';
    protected $primaryKey = 'token';
}
