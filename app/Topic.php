<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected const ACTIVE = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_en', 'name_de', 'preview'];

    /**
     * Categories of topic.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    /**
     * Advertisings of topic.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function advertising()
    {
        return $this->hasMany(Advertising::class)->where('status', self::ACTIVE);
    }

    public function places()
    {
        return $this->hasMany(Place::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function tips()
    {
        return $this->hasMany(Tip::class);
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
