<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->id();
            $table->string('name_en');
            $table->string('name_de');
            $table->text('preview');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreignId('topic_id')
                ->constrained()
                ->onDelete('cascade');
        });

        Schema::table('advertisings', function (Blueprint $table) {
            $table->foreignId('topic_id')
                ->constrained()
                ->onDelete('cascade');
        });

        Schema::table('places', function (Blueprint $table) {
            $table->foreignId('topic_id')
                ->constrained()
                ->onDelete('cascade');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->foreignId('topic_id')
                ->constrained()
                ->onDelete('cascade');
        });

        Schema::table('tips', function (Blueprint $table) {
            $table->foreignId('topic_id')
                ->constrained()
                ->onDelete('cascade');
        });

        Schema::table('services', function (Blueprint $table) {
            $table->foreignId('topic_id')
                ->constrained()
                ->onDelete('cascade');
        });

        Schema::table('shops', function (Blueprint $table) {
            $table->foreignId('topic_id')
                ->constrained()
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
