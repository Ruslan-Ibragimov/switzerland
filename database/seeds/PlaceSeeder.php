<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $places = [
            [
                'topic_id' => 1,
                'region_id' => 3,
                'video_link' => 'img/AdBanner_Polo.jpg',
                'preview' => 'img/AdBanner_Polo.jpg',
                'title' => Str::random(10),
                'address' => Str::random(30),
                'google' => '-123123, 1424124',
                'description_en' => 'Lorem ipsum delor',
                'description_de' => 'Lorem ipsum delor',
                'category_id' => 1
            ]
        ];

        collect($places)->each(function ($place) {
            DB::table('places')->insert($place);
        });
    }
}
