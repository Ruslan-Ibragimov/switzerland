<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            ['city_en' => 'Basel', 'city_de' => 'Basel'],
            ['city_en' => 'Bern', 'city_de' => 'Bern'],
            ['city_en' => 'Biel', 'city_de' => 'Biel'],
            ['city_en' => 'Geneva', 'city_de' => 'Genf'],
            ['city_en' => 'Lausanne', 'city_de' => 'Lausanne'],
            ['city_en' => 'Lugano', 'city_de' => 'Lugano'],
            ['city_en' => 'Lucerne', 'city_de' => 'Luzern'],
            ['city_en' => 'St.Gallen ', 'city_de' => 'St. Gallen'],
            ['city_en' => 'Winterthur ', 'city_de' => 'Winterthur'],
            ['city_en' => 'Zurich', 'city_de' => 'Zürich'],
        ];

        collect($cities)->each(function ($city) {
            \Illuminate\Support\Facades\DB::table('cities')->insert($city);
        });
    }
}
