<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'region_en' => 'Fribourg Region',
                'region_de' => 'Fribourg Region',
            ],
            [
                'region_en' => 'Graubünden',
                'region_de' => 'Graubünden',
            ],
            [
                'region_en' => 'Bern Region',
                'region_de' => 'Bern Region',
            ],
            [
                'region_en' => 'Geneva',
                'region_de' => 'Genf',
            ],
            [
                'region_en' => 'Lake Geneva Region / Vaud',
                'region_de' => 'Genferseegebiet / Waadtland',
            ],
            [
                'region_en' => 'Basel Region',
                'region_de' => 'Basel Region',
            ],
            [
                'region_en' => 'Valais',
                'region_de' => 'Wallis',
            ],
            [
                'region_en' => 'Ticino',
                'region_de' => 'Tessin',
            ],
            [
                'region_en' => 'Eastern Switzerland',
                'region_de' => 'Ostschweiz',
            ],
            [
                'region_en' => 'Zurich Region',
                'region_de' => 'Zürich Region',
            ],
            [
                'region_en' => 'Lucerne/ Lake Lucerne',
                'region_de' => 'Luzern/ Vierwaldstättersee',
            ],
            [
                'region_en' => 'Bernese Oberland',
                'region_de' => 'Berner Oberland',
            ],
            [
                'region_en' => 'Jura & Three-Lakes',
                'region_de' => 'Jura & Drei-Seen-Land',
            ],
        ];

        collect($regions)->each(function ($region) {
            \Illuminate\Support\Facades\DB::table('regions')->insert($region);
        });
    }
}
