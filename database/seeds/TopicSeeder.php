<?php

use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topics = [
            [
                'name_en' => 'Must See',
                'name_de' => 'Must See',
                'preview' => 'img/home_01_Mustsee.png',
                'slug' => '	must-see',
                'categories' => [
                    ['name_en' => 'Mountains', 'name_de' => 'Berge'],
                    ['name_en' => 'Castles & Palaces', 'name_de' => 'Burgen & Schlösser'],
                    ['name_en' => 'Museums', 'name_de' => 'Museen'],
                    ['name_en' => 'Landscape & Nature Parks', 'name_de' => 'Landschaft & Naturparks'],
                    ['name_en' => 'Building Culture', 'name_de' => 'Baukultur'],
                    ['name_en' => 'Churches', 'name_de' => 'Kirchen'],
                    ['name_en' => 'Culture', 'name_de' => 'Kultur'],
                    ['name_en' => 'Activities', 'name_de' => 'Aktivitäten'],
                    ['name_en' => 'Localities', 'name_de' => 'Ortschaften'],
                ]
            ],
            [
                'name_en' => 'Events & Tickets',
                'name_de' => 'Events & Tickets',
                'preview' => 'img/home_02_Tickets.png',
                'slug' => 'events-tickets',
                'categories' => [
                    ['name_en' => 'Events', 'name_de' => 'Events'],
                    ['name_en' => 'Music & Festivals Musicals', 'name_de' => 'Music & Festivals Musicals'],
                    ['name_en' => 'Opera & Theatre Sport', 'name_de' => 'Opera & Theatre Sport'],
                ]
            ],
            [
                'name_en' => 'Tips From Locals',
                'name_de' => 'Insider Tips',
                'preview' => 'img/home_03DE_Localtips.png',
                'slug' => 'local-tips',
                'categories' => [
                    ['name_en' => 'BREAKFAST, CAFÉS', 'name_de' => 'BREAKFAST, CAFÉS'],
                    ['name_en' => 'FOOD, RESTAURANTS', 'name_de' => 'FOOD, RESTAURANTS'],
                    ['name_en' => 'SHOPPING', 'name_de' => 'SHOPPING'],
                    ['name_en' => 'BARS & CLUBS', 'name_de' => 'BARS & CLUBS'],
                    ['name_en' => 'MY FAVOURITE PLACE', 'name_de' => 'MY FAVOURITE PLACE'],
                ]
            ],
            [
                'name_en' => 'Services',
                'name_de' => 'Services',
                'preview' => 'img/home_06_services.png',
                'slug' => 'services',
                'categories' => [
                    ['name_en' => 'Switzerland', 'name_de' => 'Die Schweiz'],
                    ['name_en' => 'Entry regulations', 'name_de' => 'Einreise Bestimmungen'],
                    ['name_en' => 'Railway', 'name_de' => 'Bahn'],
                    ['name_en' => 'Taxi', 'name_de' => 'Taxi'],
                    ['name_en' => 'Shipping companies', 'name_de' => 'Schiffahrtsbetriebe'],
                    ['name_en' => 'Webcams', 'name_de' => 'Webcams'],
                    ['name_en' => 'Pharmacies', 'name_de' => 'Apotheken'],
                    ['name_en' => 'Doctors', 'name_de' => 'Ärzte'],
                    ['name_en' => 'Hospitals', 'name_de' => 'Spitäler'],
                    ['name_en' => 'Embassies & Consulates', 'name_de' => 'Botschaften & Konsulate'],
                ]
            ],
            [
                'name_en' => 'Souvenir Shops',
                'name_de' => 'Souvenir Shops',
                'preview' => 'img/home_07_souvenier_shop.png',
                'slug' => 'souvenier-shop',
                'categories' => [
                    ['name_en' => 'Souvenir', 'name_de' => 'Souvenir'],
                    ['name_en' => 'Travelling with children', 'name_de' => 'Reisen mit Kinder'],
                    ['name_en' => 'Security', 'name_de' => 'Sicherheit'],
                    ['name_en' => 'Smart travel', 'name_de' => 'Smart Travel'],
                    ['name_en' => 'Electronics', 'name_de' => 'Elektronik'],
                    ['name_en' => 'Comfort', 'name_de' => 'Komfort'],
                    ['name_en' => 'Suitcases, bags & backpacks', 'name_de' => 'Koffer, Taschen & Rucksäcke'],
                    ['name_en' => 'Miscellaneous', 'name_de' => 'Sonstiges'],
                ]
            ],
        ];

        collect($topics)->each(function ($topic) {
            $created = \App\Topic::create([
                'name_en' => $topic['name_en'],
                'name_de' => $topic['name_de'],
                'preview' => $topic['preview']
            ]);

            foreach ($topic['categories'] as $category) {
                $created->categories()->create($category);
            }
        });
    }
}
