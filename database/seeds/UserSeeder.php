<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name' => 'Admin',
            'last_name' => 'Admin',
            'email' => 'root@gmail.com',
            'email_verified_at' => now(),
            'password' => \Illuminate\Support\Facades\Hash::make('ETQw753emd'),
            'remember_token' => Str::random(10),
        ]);

        \Spatie\Permission\Models\Role::create(['name' => 'admin']);

        $user->assignRole('admin');
    }
}
