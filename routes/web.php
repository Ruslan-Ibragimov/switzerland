<?php

use App\Imports\ServicesImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false
]);

Route::group(['middleware' => 'role:admin'], function () {
    Route::resource('place', 'PlaceController')->except('show');
    Route::resource('event', 'EventController')->except('show');
    Route::resource('email-banner', 'EmailBannerController')->except('show');
    Route::resource('tip', 'TipController')->except('show');
    Route::resource('service', 'ServiceController')->except('show');
    Route::resource('shop', 'ShopController')->except('show');
    Route::resource('ad', 'AdvertisingController')->except('show');
    Route::resource('user', 'UserController')->except('show');
    Route::resource('author', 'AuthorController')->except('show');
    Route::get('logout', 'Auth\AuthController@logout')->name('logout');


    Route::get('diactivate/place/{place}', 'PlaceController@diactivate')->name('place.diactivate');
    Route::get('diactivate/email-banner/{emailBanner}', 'EmailBannerController@diactivate')->name('email-banner.diactivate');
    Route::get('diactivate/event/{event}', 'EventController@diactivate')->name('event.diactivate');
    Route::get('diactivate/tip/{tip}', 'TipController@diactivate')->name('tip.diactivate');
    Route::get('diactivate/service/{service}', 'ServiceController@diactivate')->name('service.diactivate');
    Route::get('diactivate/shop/{shop}', 'ShopController@diactivate')->name('shop.diactivate');
    Route::get('diactivate/ad/{ad}', 'AdvertisingController@diactivate')->name('ad.diactivate');

    // Excel export
    Route::get('user/export', 'UserController@export')->name('user.export');
    Route::get('/excel-imports', function () {
        return view('import');
    });

    Route::post('import-data', function (Request $request) {
        Excel::import(new ServicesImport, $request->file('file'));
        return back();
    })->name('import');
});

Route::get('password/reset', 'Auth\AuthController@resetPassword')->name('reset.password');
Route::post('password/update', 'Auth\AuthController@updatePassword')->name('update.password');
Route::get('account/activate', 'Auth\AuthController@accountActivate')->name('activate.account');
Route::get('redirect/{token}', 'RedirectController@index')->name('redirect');
Route::get('/welcome', 'RedirectController@index');

Route::get('/bucket-list/{token}', 'UserController@bucketMap')->name('bucket.list');


Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('home');
});

Route::get('/', function () {
    return redirect()->route('login');
});


Route::get('fake/users', function () {
    factory(App\User::class, 100)->create();
});
