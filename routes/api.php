<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'localate', 'as' => 'api.'], function () {
    // Auth
    Route::post('login', 'API\Auth\LoginController@login')->name('login');
    Route::post('logout', 'API\Auth\LoginController@logout')->middleware('auth:sanctum');
    Route::post('register', 'API\Auth\RegisterController@register')->name('register');
    Route::post('forget-password', 'API\Auth\RegisterController@forget')->name('forget');

    // Bucket
    Route::apiResource('bucket', 'API\BucketController')->only('index', 'destroy')->middleware('auth:sanctum');
    Route::post('export-bucket', 'API\BucketController@exportBucket')->name('bucket-export')->middleware('auth:sanctum');

    // Entries
    Route::apiResource('places', 'API\PlaceController')->except('update', 'delete');
    Route::apiResource('service', 'API\ServiceController')->except('update', 'delete');
    Route::apiResource('shops', 'API\ShopController')->except('update', 'delete');
    Route::apiResource('events', 'API\EventController')->except('update', 'delete');
    Route::apiResource('tips', 'API\TipController')->except('update', 'delete');

    // Topics
    Route::apiResource('topic', 'API\TopicController')->only('index', 'show');

    Route::get('must-see', 'API\PlaceController@main');
    Route::get('events-tickets', 'API\EventController@main');
    Route::get('souvenier-shop', 'API\ShopController@main');

    Route::get('local-tips', 'API\TipController@main');
    Route::get('local-tips/author/{author}', 'API\TipController@author');

    Route::get('services', 'API\ServiceController@main');
    Route::post('support ', 'API\SupportController@userRequest');

    Route::post('weblink-count/{advertising}', 'API\BucketController@countWeblink');



    Route::group(['prefix' => 'topic/{topic}'], function () {
        Route::get('place', 'API\TopicController@place');
        Route::get('service', 'API\TopicController@service');
        Route::get('tip', 'API\TopicController@tip');
        Route::get('shop', 'API\TopicController@shop');
        Route::get('event', 'API\TopicController@event');
    });

    // Regions
    Route::get('regions', 'API\RegionController@index');

    // Cities
    Route::get('cities', 'API\CityController@index');
});



Route::get('unauthorized', function () {
    return response()->json([
        'success' => false,
        'result' => (object) [],
        'error' => (object) ['msg' => 'Unauthorized']
    ], 401);
})->name('unauthorized');


Route::fallback(function () {
    return response()->json([
        'success' => false,
        'result' => (object) [],
        'error' => (object) ['msg' => 'Route not found']
    ], 401);
});


Route::get('clear-cache', function () {
    $exitCode = \Illuminate\Support\Facades\Artisan::call('cache:clear');
    $exitCode = \Illuminate\Support\Facades\Artisan::call('config:cache');
    $exitCode = \Illuminate\Support\Facades\Artisan::call('config:clear');
    $exitCode = \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Session::flush();
    return $exitCode;
});
