@extends('layouts.bucket_map')
<style>
    #bucketMap {
        height: 100%;
    }
</style>
@section('content')
<div id="bucketMap"></div>
@endsection

@section('javascript')
<script>
    let data = @json($data->geo_info);
    let icon = L.icon({
        iconUrl: '{{ asset('img/icons/marker.png') }}',
        iconSize:     [38, 40], 
        iconAnchor:   [22, 40], 
        popupAnchor:  [-4, -44] 
    });

    let first = data[Object.keys(data)[0]][0];
    const bucketMap = L.map('bucketMap').setView([first.lat, first.lng], 8);
    const attribution ='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
    const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    const tiles = L.tileLayer(tileUrl, { attribution });
    tiles.addTo(bucketMap);
    let index = 0;
    let markers = [];
    for (const topic in data) {
        data[topic].map((item,key) => {
            let popupText = `
            <b>${item.title}</b>
            <p class="my-0">${item.address}</p>`;
            // <p class="my-0 mt-2 text-right">${item.created_at}</p>
            let marker = L.marker([item.lat,item.lng], {icon: icon}).addTo(bucketMap)
                .bindPopup(popupText).on('click', handleMarket);
            marker.guid = `${topic}_${item.id}`;
            
            if(index == 0 && key == 0){
               marker.openPopup();
            }
            index++;
            markers.push(marker);
        });
    }
    let navs = document.querySelectorAll('.map-navbar ul li');
    let toggler = document.querySelector('.toggler');

    navs.forEach(nav => {
        nav.addEventListener('click',function (e) { 
            getToMarker(e);
         });
    });

    function getToMarker(el){
        let guid = el.target.dataset.guid;
        let marker = markers.find(marker => marker.guid == guid);
        if(marker){
            let coords = marker.getLatLng();
            bucketMap.setView([coords.lat, coords.lng], 8);
            marker.openPopup();
        }
        removeActiveClass();
        el.target.classList.add('active');
        document.querySelector('.map-navbar').classList.remove('navbar-active');
        toggler.classList.remove('toggler-diactive');
        
    }

    function removeActiveClass(){
        navs.forEach(nav => {
            nav.classList.remove("active");
        });
    }

    function addActiveClass(el){
        let node = document.querySelector('.map-navbar ul li[data-guid='+el.guid+']');
        node.classList.add('active');
        node.scrollIntoView();
    }

    function handleMarket(e) {
        removeActiveClass();
        addActiveClass(e.target);
    }
    toggler.addEventListener('click',activateNavbar);
    function activateNavbar(){
        document.querySelector('.map-navbar').classList.add('navbar-active');
        toggler.classList.add('toggler-diactive');
    }
</script>
@endsection