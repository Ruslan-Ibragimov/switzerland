@extends('layouts.admin')

@section('content')
  <form action="{{route('tip.store')}}" method="post" enctype="multipart/form-data">
  @csrf
    <!-- Author -->
    <div class="form-group row">
      <label for="input4" class="col-sm-2 col-form-label text-uppercase">Author</label>

      <div class="col-sm-9">
        <select id="input4" class="custom-select @error('author_id') is-invalid @enderror" name="author_id" required>
          <option value=""></option>
          @foreach($authors as $author)
            <option value="{{$author->id}}" @if(old('author_id') == $author->id) selected @endif>{{$author->name}}</option>
          @endforeach
        </select>

        @error('author_id')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End Author -->

    <!-- City -->
    <div class="form-group row">
      <label for="input5" class="col-sm-2 col-form-label text-uppercase">City</label>

      <div class="col-sm-9">
        <select id="input5" class="custom-select @error('city_id') is-invalid @enderror" name="city_id" required>
          <option value=""></option>
          @foreach($cities as $city)
            <option value="{{$city->id}}" @if(old('city_id') == $city->id) selected @endif>{{$city->city_de}}</option>
          @endforeach
        </select>

        @error('city_id')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End City -->

    <!-- Rubrik -->
    <div class="form-group row">
      <label for="input6" class="col-sm-2 col-form-label text-uppercase">Rubrik</label>

      <div class="col-sm-9">
        <select id="input6" class="custom-select @error('category_id') is-invalid @enderror" name="category_id" required>
          <option value=""></option>
          @foreach($categories as $category)
            <option value="{{$category->id}}" @if(old('category_id') == $category->id) selected @endif>{{$category->name_de}}</option>
          @endforeach
        </select>

        @error('category_id')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End Rubrik -->

    <!-- Preview -->
    <div class="form-group row">
      <label for="input" class="col-sm-2 col-form-label text-uppercase">Preview</label>

      <div class="col-sm-9">
        <div class="custom-file">
          <input type="file" class="custom-file-input @error('preview') is-invalid @enderror" id="inputGroupFile01" name="preview" aria-describedby="inputGroupFileAddon01" required>
          <small class="text-muted">512X800</small>

          <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

          @error('preview')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>
    </div>
    <!-- End Preview -->

    <!-- Titel -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Titel/Frim</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('title') is-invalid @enderror" name="title" value="{{old('title')}}" required>

        @error('title')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
        @enderror
      </div>
    </div>
    <!-- End Titel -->

    <!-- Adresse -->
    <div class="form-group row">
      <label for="input3" class="col-sm-2 col-form-label text-uppercase">Adresse</label>

      <div class="col-sm-9">
        <textarea id="input3" class="form-control @error('address') is-invalid @enderror" name="address" required>{{old('address')}}</textarea>

        @error('address')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
        @enderror
      </div>
    </div>
    <!-- End Adresse -->

    <!-- Google -->
    <div class="form-group row">
      <label for="input4" class="col-sm-2 col-form-label text-uppercase">Google</label>

      <div class="col-sm-9">
        <input type="text" id="input4" class="form-control @error('google') is-invalid @enderror" name="google" value="{{old('google')}}" required>

        @error('google')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
         </span>
        @enderror
      </div>
    </div>
    <!-- End Google -->

    <!-- BeschriebDeutsch -->
    <div class="form-group row">
      <label for="input7" class="col-sm-2 col-form-label text-uppercase">BeschriebDeutsch</label>

      <div class="col-sm-9">
        <textarea id="input7" class="form-control @error('description_de') is-invalid @enderror" rows="4" name="description_de" required>{{old('description_de')}}</textarea>

        @error('description_de')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
        @enderror
      </div>
    </div>
    <!-- End BeschriebDeutsch -->

    <!-- DescriptionEnglish -->
    <div class="form-group row">
      <label for="input8" class="col-sm-2 col-form-label text-uppercase">DescriptionEnglish</label>

      <div class="col-sm-9">
        <textarea id="input8" class="form-control @error('description_en') is-invalid @enderror" rows="4" name="description_en" required>{{old('description_en')}}</textarea>

        @error('description_en')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
        @enderror
      </div>
    </div>
    <!-- End DescriptionEnglish -->

    <!-- Weblink -->
    <div class="form-group row">
      <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink</label>

      <div class="col-sm-9">
        <input type="url" id="input9" class="form-control @error('weblink') is-invalid @enderror" name="weblink" value="{{old('weblink')}}">

        @error('weblink')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End Weblink -->

    <!-- Button -->
    <div class="form-group row mt-5">
      <div class="col-sm-7">
        <button type="submit" class="btn btn-danger btn-block text-left">EINTRAG SPEICHERN</button>
      </div>
    </div>
    <!-- End Button -->
  </form>
@endsection
