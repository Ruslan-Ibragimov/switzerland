@extends('layouts.admin')

@section('content')
  <form action="{{route('author.store')}}" method="post" enctype="multipart/form-data">
  @csrf

    <!-- Photo -->
    <div class="form-group row">
      <label for="input" class="col-sm-2 col-form-label text-uppercase">Photo</label>

      <div class="col-sm-9">
        <div class="custom-file">
          <input type="file" class="custom-file-input @error('photo') is-invalid @enderror" id="inputGroupFile01" name="photo" aria-describedby="inputGroupFileAddon01" required>
          <small class="text-muted">512X512</small>

          <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

          @error('photo')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>
    </div>
    <!-- End Photo -->

    <!-- Name -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Vorname/Name</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name')}}" required>

        @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End Name -->

    <!-- Profession En -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Proffession En</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('proffession_en') is-invalid @enderror" name="proffession_en" value="{{old('proffession_en')}}" required>

        @error('proffession_en')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- Profession En -->

    <!-- Profession De -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Beruf De</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('proffession_de') is-invalid @enderror" name="proffession_de" value="{{old('proffession_de')}}" required>

        @error('proffession_de')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End Profession De -->

    <!-- Button -->
    <div class="form-group row mt-5">
      <div class="col-sm-7">
        <button type="submit" class="btn btn-danger btn-block text-left">EINTRAG SPEICHERN</button>
      </div>
    </div>
    <!-- End Button -->
  </form>
@endsection
