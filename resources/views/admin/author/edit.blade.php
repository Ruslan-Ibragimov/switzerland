@extends('layouts.admin')

@section('content')
  <div class="row">
    <div class="col-sm-11">
      <img class="mb-4" src="{{asset($author->photo)}}" width="128px" height="128px" alt="Image Description">
    </div>
  </div>

  <form action="{{route('author.update', $author->id)}}" method="post" enctype="multipart/form-data">
    @csrf

    @method('put')

    <!-- Photo -->
    <div class="form-group row">
      <label for="input" class="col-sm-2 col-form-label text-uppercase">Photo</label>

      <div class="col-sm-9">
        <div class="custom-file">
          <input type="file" class="custom-file-input @error('photo') is-invalid @enderror" id="inputGroupFile01" name="photo" aria-describedby="inputGroupFileAddon01">
          <small class="text-muted">512X512</small>

          <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

          @error('photo')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>
    </div>
    <!-- End Photo -->

    <!-- Name -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Vorname/Name</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name') ? old('name') : $author->name}}" required>

        @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End Name -->

    <!-- Profession En -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Proffession En</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('proffession_en') is-invalid @enderror" name="proffession_en" value="{{old('proffession_en') ? old('proffession_en') : $author->proffession_en}}" required>

        @error('proffession_en')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- Profession En -->

    <!-- Profession De -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Beruf De</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('proffession_de') is-invalid @enderror" name="proffession_de" value="{{old('proffession_de') ? old('proffession_de') : $author->proffession_de}}" required>

        @error('proffession_de')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
    <!-- End Profession De -->

    <!-- Button -->
    <div class="form-group row mt-5">
      <div class="col-sm-7">
        <div class="d-flex">
          <button type="submit" class="btn btn-danger btn-block text-left mr-2">EINTRAG SPEICHERN</button>

          <button class="btn btn-danger" form="deleteForm">
            <img src="{{asset('img/icons/ebene.png')}}" alt="Image Description">
          </button>
        </div>
      </div>
    </div>
    <!-- End Button -->
  </form>

  <form id="deleteForm" action="{{route('author.destroy', $author->id)}}" method="post">
    @csrf
    @method('delete')
  </form>
@endsection
