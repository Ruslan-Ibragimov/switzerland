@extends('layouts.admin')

@section('content')
<form id="filterForm" action="" class="mb-4">
  <div class="row">
    <div class="col-sm-3 offset-sm-9">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Suche" name="vorname" aria-label="Recipient's username"
          aria-describedby="button-addon2">
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit" id="button-addon2">&#8594;</button>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="table-responsive">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th class="text-uppercase" scope="col">Id</th>
        <th scope="col">Photo</th>
        <th scope="col">Vorname</th>
        <th scope="col">Proffession</th>
        <th class="text-center text-uppercase" scope="col">BEARBEITEN</th>
        <th class="text-center text-uppercase" scope="col">Löschen</th>
      </tr>
    </thead>
    <tbody>
      @foreach($authors as $author)
      <tr>
        <td>{{$author->id}}</td>
        <td>
          <img class="img-fluid rounded-circle" width="40px" src="{{asset($author->photo)}}" alt="{{$author->name}}">
        </td>
        <td>{{$author->name}}</td>
        <td>{{$author->proffession_de}}</td>
        <td class="text-center">
          <a href="{{route('author.edit', $author->id)}}">
            <img src="{{asset('img/icons/edit.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="#" class="delete" data-id="{{ $author->id }}" data-slug="author" data-toggle="modal"
            data-target="#deleteModal">
            <img src="{{asset('img/icons/delete.png')}}" alt="Image Description">
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="row">
  <div class="col-sm-3 mb-3 mb-sm-3">
    <a class="btn btn-danger btn-block text-left" href="{{route('author.create')}}">EINTRAG ERFASSEN</a>
  </div>

  <div class="col-sm-9">
    {{$authors->links()}}
  </div>
</div>
@endsection