@extends('layouts.admin')

@section('content')
<form action="{{route('service.store')}}" method="post" enctype="multipart/form-data">
    @csrf

    <!-- Preview -->
    <div class="form-group row">
        <label for="input" class="col-sm-2 col-form-label text-uppercase">Photo preview</label>

        <div class="col-sm-9">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('preview') is-invalid @enderror"
                    id="inputGroupFile01" name="preview" aria-describedby="inputGroupFileAddon01" required>
                <small class="text-muted">1120X610</small>

                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

                @error('preview')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
    </div>
    <!-- End Preview -->

    <!-- Video link -->
    <div class="form-group row">
        <label for="input1" class="col-sm-2 col-form-label text-uppercase">Video link</label>

        <div class="col-sm-9">
            <input type="url" id="input1" class="form-control @error('video_link') is-invalid @enderror"
                name="video_link" value="{{old('video_link')}}">

            @error('video_link')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Video link -->

    <!-- Titel En -->
    <div class="form-group row">
        <label for="input2" class="col-sm-2 col-form-label text-uppercase">Titel English</label>

        <div class="col-sm-9">
            <input type="text" id="input2" class="form-control @error('title_en') is-invalid @enderror" name="title_en"
                value="{{old('title_en')}}" required>

            @error('title_en')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Titel En -->

    <!-- Titel De -->
    <div class="form-group row">
        <label for="input2" class="col-sm-2 col-form-label text-uppercase">Titel Deutsch</label>

        <div class="col-sm-9">
            <input type="text" id="input2" class="form-control @error('title_de') is-invalid @enderror" name="title_de"
                value="{{old('title_de')}}" required>

            @error('title_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Titel De -->
    <!-- Rubrik -->
    {{-- <div class="form-group row">
      <label for="input6" class="col-sm-2 col-form-label text-uppercase">Rubrik</label>

      <div class="col-sm-9">
        <select id="input6" class="custom-select @error('category_id') is-invalid @enderror" name="category_id" required>
          <option value=""></option>
          @foreach($categories as $category)
            <option value="{{$category->id}}" @if(old('category_id') == $category->id) selected
    @endif>{{$category->name_de}}</option>
    @endforeach
    </select>

    @error('category_id')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
    </div>
    </div> --}}
    <!-- End Rubrik -->

    <!-- BeschriebDeutsch -->
    <div class="form-group row">
        <label for="input7" class="col-sm-2 col-form-label text-uppercase">BeschriebDeutsch</label>

        <div class="col-sm-9">
            <textarea id="input7" class="form-control @error('description_de') is-invalid @enderror" rows="4"
                name="description_de" required>{{old('description_de')}}</textarea>

            @error('description_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End BeschriebDeutsch -->

    <!-- DescriptionEnglish -->
    <div class="form-group row">
        <label for="input8" class="col-sm-2 col-form-label text-uppercase">DescriptionEnglish</label>

        <div class="col-sm-9">
            <textarea id="input8" class="form-control @error('description_en') is-invalid @enderror" rows="4"
                name="description_en" required>{{old('description_en')}}</textarea>

            @error('description_en')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End DescriptionEnglish -->

    <!-- Weblink -->
    <div class="form-group row">
        <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink</label>

        <div class="col-sm-9">
            <input type="url" id="input9" class="form-control @error('weblink') is-invalid @enderror" name="weblink"
                value="{{old('weblink')}}">

            @error('weblink')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Weblink -->

    <!-- Weblink DE -->
    <div class="form-group row">
        <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink Deutsch</label>

        <div class="col-sm-9">
            <input type="url" id="input9" class="form-control @error('weblink_de') is-invalid @enderror"
                name="weblink_de" value="{{old('weblink_de')}}">

            @error('weblink_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Weblink -->

    <!-- Facebook -->
    <div class="form-group row">
        <label for="input10" class="col-sm-2 col-form-label text-uppercase">Facebook</label>

        <div class="col-sm-9">
            <input type="url" id="input10" class="form-control @error('facebook') is-invalid @enderror" name="facebook"
                value="{{old('facebook')}}">

            @error('facebook')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Facebook -->

    <!-- Instagram -->
    <div class="form-group row">
        <label for="input11" class="col-sm-2 col-form-label text-uppercase">Instagram</label>

        <div class="col-sm-9">
            <input type="url" id="input11" class="form-control @error('instagram') is-invalid @enderror"
                name="instagram" value="{{old('instagram')}}">

            @error('instagram')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Instagram -->

    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (IOS)</label>

        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp') is-invalid @enderror" name="app_tipp"
                value="{{old('app_tipp')}}">

            @error('app_tipp')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->

    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (IOS) Deutsch</label>

        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp_de') is-invalid @enderror"
                name="app_tipp_de" value="{{old('app_tipp_de')}}">

            @error('app_tipp_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->

    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (Android)</label>

        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp_android') is-invalid @enderror"
                name="app_tipp_android" value="{{old('app_tipp_android')}}">

            @error('app_tipp_android')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->

    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (Android) Deutsch</label>

        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp_android_de') is-invalid @enderror"
                name="app_tipp_android_de" value="{{old('app_tipp_android_de')}}">

            @error('app_tipp_android_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->



    <!-- Telefon -->
    <div class="form-group row">
        <label for="input13" class="col-sm-2 col-form-label text-uppercase">Telefon</label>

        <div class="col-sm-9">
            <input type="tel" id="input13" class="form-control @error('phone') is-invalid @enderror" name="phone"
                value="{{old('phone')}}">

            @error('phone')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Telefon -->

    <!-- Button -->
    <div class="form-group row mt-5">
        <div class="col-sm-7">
            <button type="submit" class="btn btn-danger btn-block text-left">EINTRAG SPEICHERN</button>
        </div>
    </div>
    <!-- End Button -->
</form>
@endsection