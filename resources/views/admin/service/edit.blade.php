@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-sm-11">
        <img class="img-fluid mb-4" src="{{asset($service->preview)}}" alt="Image Description">
    </div>
</div>

<form action="{{route('service.update', $service->id)}}" method="post" enctype="multipart/form-data">
    @csrf

    @method('put')

    <!-- Preview -->
    <div class="form-group row">
        <label for="input" class="col-sm-2 col-form-label text-uppercase">Photo preview</label>

        <div class="col-sm-9">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('preview') is-invalid @enderror"
                    id="inputGroupFile01" name="preview" aria-describedby="inputGroupFileAddon01">
                <small class="text-muted">1120X610</small>

                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

                @error('preview')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
    </div>
    <!-- End Preview -->

    <!-- Video link -->
    <div class="form-group row">
        <label for="input1" class="col-sm-2 col-form-label text-uppercase">Video link</label>

        <div class="col-sm-9">
            <input type="url" id="input1" class="form-control @error('video_link') is-invalid @enderror"
                name="video_link" value="{{old('video_link') ? old('video_link') : $service->video_link}}">

            @error('video_link')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Video link -->

    <!-- Titel En -->
    <div class="form-group row">
        <label for="input2" class="col-sm-2 col-form-label text-uppercase">Titel English</label>

        <div class="col-sm-9">
            <input type="text" id="input2" class="form-control @error('title_en') is-invalid @enderror" name="title_en"
                value="{{old('title_en') ? old('title_en') : $service->title_en}}" required>

            @error('title_en')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Titel En -->

    <!-- Titel De -->
    <div class="form-group row">
        <label for="input2" class="col-sm-2 col-form-label text-uppercase">Titel Deutsch</label>

        <div class="col-sm-9">
            <input type="text" id="input2" class="form-control @error('title_de') is-invalid @enderror" name="title_de"
                value="{{old('title_de') ? old('title_de') : $service->title_de}}" required>

            @error('title_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Titel De -->

    <!-- Rubrik -->
    {{-- <div class="form-group row">
      <label for="input6" class="col-sm-2 col-form-label text-uppercase">Rubrik</label>

      <div class="col-sm-9">
        <select id="input6" class="custom-select @error('category_id') is-invalid @enderror" name="category_id" required>
          <option value=""></option>
          @foreach($categories as $category)
            <option value="{{$category->id}}" @if($service->category_id == $category->id) selected
    @endif>{{$category->name_de}}</option>
    @endforeach
    </select>

    @error('category_id')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
    </div>
    </div> --}}
    <!-- End Rubrik -->

    <!-- BeschriebDeutsch -->
    <div class="form-group row">
        <label for="input7" class="col-sm-2 col-form-label text-uppercase">BeschriebDeutsch</label>

        <div class="col-sm-9">
            <textarea id="input7" class="form-control @error('description_de') is-invalid @enderror" rows="4"
                name="description_de"
                required>{{old('description_de') ? old('description_de') : $service->description_de}}</textarea>

            @error('description_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End BeschriebDeutsch -->

    <!-- DescriptionEnglish -->
    <div class="form-group row">
        <label for="input8" class="col-sm-2 col-form-label text-uppercase">DescriptionEnglish</label>

        <div class="col-sm-9">
            <textarea id="input8" class="form-control @error('description_en') is-invalid @enderror" rows="4"
                name="description_en"
                required>{{old('description_en') ? old('description_en') : $service->description_en}}</textarea>

            @error('description_en')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End DescriptionEnglish -->

    <!-- Weblink -->
    <div class="form-group row">
        <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink</label>

        <div class="col-sm-9">
            <input type="url" id="input9" class="form-control @error('weblink') is-invalid @enderror" name="weblink"
                value="{{old('weblink') ? old('weblink') : $service->weblink}}">

            @error('weblink')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Weblink -->

    <!-- Weblink_DE -->
    <div class="form-group row">
        <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink Deutsch</label>

        <div class="col-sm-9">
            <input type="url" id="input9" class="form-control @error('weblink_de') is-invalid @enderror"
                name="weblink_de" value="{{old('weblink_de') ? old('weblink_de') : $service->weblink_de}}">

            @error('weblink_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Weblink -->

    <!-- Facebook -->
    <div class="form-group row">
        <label for="input10" class="col-sm-2 col-form-label text-uppercase">Facebook</label>

        <div class="col-sm-9">
            <input type="url" id="input10" class="form-control @error('facebook') is-invalid @enderror" name="facebook"
                value="{{old('facebook') ? old('facebook') : $service->facebook}}">

            @error('facebook')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Facebook -->

    <!-- Instagram -->
    <div class="form-group row">
        <label for="input11" class="col-sm-2 col-form-label text-uppercase">Instagram</label>

        <div class="col-sm-9">
            <input type="url" id="input11" class="form-control @error('instagram') is-invalid @enderror"
                name="instagram" value="{{old('instagram') ? old('instagram') : $service->instagram}}">

            @error('instagram')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Instagram -->

    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (IOS)</label>

        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp') is-invalid @enderror" name="app_tipp"
                value="{{old('app_tipp') ? old('app_tipp') : $service->app_tipp}}">

            @error('app_tipp')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->
    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (IOS) Deutsch</label>

        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp_de') is-invalid @enderror"
                name="app_tipp_de" value="{{old('app_tipp_de') ? old('app_tipp_de') : $service->app_tipp_de}}">

            @error('app_tipp_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->

    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (Android)</label>


        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp_android') is-invalid @enderror"
                name="app_tipp_android"
                value="{{old('app_tipp_android') ? old('app_tipp_android') : $service->app_tipp_android}}">

            @error('app_tipp_android')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->
    <!-- App Tipp -->
    <div class="form-group row">
        <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (Android) Deutsch</label>


        <div class="col-sm-9">
            <input type="url" id="input12" class="form-control @error('app_tipp_android_de') is-invalid @enderror"
                name="app_tipp_android_de"
                value="{{old('app_tipp_android_de') ? old('app_tipp_android_de') : $service->app_tipp_android_de}}">

            @error('app_tipp_android_de')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End App Tipp -->

    <!-- Telefon -->
    <div class="form-group row">
        <label for="input13" class="col-sm-2 col-form-label text-uppercase">Telefon</label>

        <div class="col-sm-9">
            <input type="tel" id="input13" class="form-control @error('phone') is-invalid @enderror" name="phone"
                value="{{old('phone') ? old('phone') : $service->phone}}">

            @error('phone')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- End Telefon -->

    <!-- Button -->
    <div class="form-group row mt-5">
        <div class="col-sm-7">
            <div class="d-flex">
                <button type="submit" class="btn btn-danger btn-block text-left mr-2">EINTRAG SPEICHERN</button>

                <button class="btn btn-danger" form="deleteForm">
                    <img src="{{asset('img/icons/ebene.png')}}" alt="Image Description">
                </button>
            </div>
        </div>
    </div>
    <!-- End Button -->
</form>

<form id="deleteForm" action="{{route('service.destroy', $service->id)}}" method="post">
    @csrf
    @method('delete')
</form>
@endsection