@extends('layouts.admin')

@section('content')
<form id="filterForm" action="" class="mb-4">
  <div class="row">
    <div class="col-sm-3 offset-sm-6 mb-3 mb-sm-0">
      <select id="categoriesFilter" class="custom-select" name="category">
        <option value="">All Kategorie</option>
        @foreach($categories as $category)
        <option value="{{$category->id}}" @if(request()->category == $category->id) selected
          @endif>{{$category->name_de}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-3">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Suche" name="firma" aria-label="Recipient's username"
          aria-describedby="button-addon2">
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit" id="button-addon2">&#8594;</button>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="table-responsive">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th class="text-uppercase" scope="col">Id</th>
        <th scope="col">Firma</th>
        <th scope="col">Kategorie</th>
        <th scope="col">Aktiv bis</th>
        <th scope="col">Anzahl Views</th>
        <th scope="col">Anzahl Merkliste</th>
        <th scope="col">Status</th>
        <th class="text-center text-uppercase" scope="col">BEARBEITEN</th>
        <th class="text-center text-uppercase" scope="col">Löschen</th>
      </tr>
    </thead>
    <tbody>
      @foreach($shops as $shop)
      <tr>
        <td>{{$shop->id}}</td>
        <td>{{$shop->firma}}</td>
        <td>{{$shop->categories->name_de}}</td>
        <td>{{$shop->valid_to}}</td>
        <td>{{$shop->getActivityCount('detail')}}</td>
        <td>{{$shop->getWatchListCount()}}</td>
        <td>
          <a href="{{route('shop.diactivate', $shop->id)}}">
            <img src="{{asset('img/icons/status-' . $shop->status . '.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="{{route('shop.edit', $shop->id)}}">
            <img src="{{asset('img/icons/edit.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="#" class="delete" data-id="{{ $shop->id }}" data-slug="shop" data-toggle="modal"
            data-target="#deleteModal">
            <img src="{{asset('img/icons/delete.png')}}" alt="Image Description">
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="row">
  <div class="col-sm-3 mb-3 mb-sm-3">
    <a class="btn btn-danger btn-block text-left" href="{{route('shop.create')}}">EINTRAG ERFASSEN</a>
  </div>

  <div class="col-sm-9">
    {{$shops->links()}}
  </div>
</div>
@endsection