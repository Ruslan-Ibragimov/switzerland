@extends('layouts.admin')

@section('content')
  <div class="row">
    <div class="col-sm-11">
      <img class="img-fluid mb-4" src="{{asset($shop->preview)}}" width="336px" height="226px" alt="Image Description">
    </div>
  </div>

  <form action="{{route('shop.update', $shop->id)}}" method="post" enctype="multipart/form-data">
  @csrf

  @method('put')

  <!-- Preview -->
    <div class="form-group row">
      <label for="input" class="col-sm-2 col-form-label text-uppercase">Photo preview</label>

      <div class="col-sm-9">
        <div class="custom-file">
          <input type="file" class="custom-file-input @error('preview') is-invalid @enderror" id="inputGroupFile01" name="preview" aria-describedby="inputGroupFileAddon01">
          <small class="text-muted">1120X750</small>

          <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

          @error('preview')
          <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
          @enderror
        </div>
      </div>
    </div>
    <!-- End Preview -->

    <!-- Firma -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Firma</label>

      <div class="col-sm-9">
        <input type="text" id="input2" class="form-control @error('firma') is-invalid @enderror" name="firma" value="{{old('firma') ? old('firma') : $shop->firma}}" required>

        @error('firma')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Firma -->

    <!-- Weblink Title -->
    <div class="form-group row">
      <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink Title</label>

      <div class="col-sm-9">
        <input type="text" id="input9" class="form-control @error('weblink_title') is-invalid @enderror" name="weblink_title" value="{{old('weblink_title') ? old('weblink_title') : $shop->weblink_title}}">

        @error('weblink_title')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Weblink Title -->

    <!-- Weblink -->
    <div class="form-group row">
      <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink</label>

      <div class="col-sm-9">
        <input type="url" id="input9" class="form-control @error('weblink') is-invalid @enderror" name="weblink" value="{{old('weblink') ? old('weblink') : $shop->weblink}}">

        @error('weblink')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Weblink -->

    <!-- Rubrik -->
    <div class="form-group row">
      <label for="input6" class="col-sm-2 col-form-label text-uppercase">Rubrik</label>

      <div class="col-sm-9">
        <select id="input6" class="custom-select @error('category_id') is-invalid @enderror" name="category_id" required>
          <option value=""></option>
          @foreach($categories as $category)
            <option value="{{$category->id}}" @if($shop->category_id == $category->id) selected @endif>{{$category->name_de}}</option>
          @endforeach
        </select>

        @error('category_id')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Rubrik -->

    <!-- BeschriebDeutsch -->
    <div class="form-group row">
      <label for="input7" class="col-sm-2 col-form-label text-uppercase">BeschriebDeutsch</label>

      <div class="col-sm-9">
        <textarea id="input7" class="form-control @error('description_de') is-invalid @enderror" rows="4" name="description_de" required>{{old('description_de') ? old('description_de') : $shop->description_de}}</textarea>

        @error('description_de')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End BeschriebDeutsch -->

    <!-- DescriptionEnglish -->
    <div class="form-group row">
      <label for="input8" class="col-sm-2 col-form-label text-uppercase">DescriptionEnglish</label>

      <div class="col-sm-9">
        <textarea id="input8" class="form-control @error('description_en') is-invalid @enderror" rows="4" name="description_en" required>{{old('description_en') ? old('description_en') : $shop->description_en}}</textarea>

        @error('description_en')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End DescriptionEnglish -->

    <!-- Date -->
    <div class="form-group row">
      <label class="col-sm-2 col-form-label text-uppercase">Gültig</label>

      <div class="col-sm-4">
        <input type="date" class="form-control @error('valid_from') is-invalid @enderror" name="valid_from" value="{{old('valid_from') ? old('valid_from') : $shop->valid_from}}">

        @error('valid_from')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>

      <div class="col-sm-4">
        <input type="date" class="form-control @error('valid_to') is-invalid @enderror" name="valid_to" value="{{old('valid_to') ? old('valid_to') : $shop->valid_to}}">

        @error('valid_to')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Date -->

    <!-- Button -->
    <div class="form-group row mt-5">
      <div class="col-sm-7">
        <div class="d-flex">
          <button type="submit" class="btn btn-danger btn-block text-left mr-2">EINTRAG SPEICHERN</button>

          <button class="btn btn-danger" form="deleteForm">
            <img src="{{asset('img/icons/ebene.png')}}" alt="Image Description">
          </button>
        </div>
      </div>
    </div>
    <!-- End Button -->
  </form>

  <form id="deleteForm" action="{{route('shop.destroy', $shop->id)}}" method="post">
    @csrf
    @method('delete')
  </form>
@endsection
