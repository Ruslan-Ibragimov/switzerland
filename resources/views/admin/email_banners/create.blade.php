@extends('layouts.admin')

@section('content')
<form action="{{route('email-banner.store')}}" method="post" enctype="multipart/form-data">
  @csrf

  <!-- Img -->
  <div class="form-group row">
    <label for="input" class="col-sm-2 col-form-label text-uppercase">Bannerbild</label>

    <div class="col-sm-9">
      <div class="custom-file">
        <input required type="file" class="custom-file-input @error('img') is-invalid @enderror" id="inputGroupFile01"
          name="img" aria-describedby="inputGroupFileAddon01">
        <small class="text-muted">832 x 276</small>

        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

        @error('img')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
  </div>
  <!-- End Img -->

  <!-- E-Mail-Typ -->
  <div class="form-group row">
    <label for="input6" class="col-sm-2 col-form-label text-uppercase">E-Mail-Typ</label>

    <div class="col-sm-9">
      <select id="input6" class="custom-select @error('type') is-invalid @enderror" name="type" required>
        <option value=""></option>
        @foreach($types as $type)
        <option value="{{$type}}" @if(old('type')==$type) selected @endif>{{$type}}
        </option>
        @endforeach
      </select>

      @error('type')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End E-Mail-Typ -->

  <!-- E-Mail-Sprache -->
  <div class="form-group row">
    <label for="input6" class="col-sm-2 col-form-label text-uppercase">E-Mail-Sprache</label>

    <div class="col-sm-9">
      <select id="input6" class="custom-select @error('lang') is-invalid @enderror" name="lang" required>
        <option value=""></option>
        @foreach($langs as $lang)
        <option value="{{$lang}}" @if(old('lang')==$lang) selected @endif>{{$lang}}
        </option>
        @endforeach
      </select>

      @error('lang')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End E-Mail-Sprache -->

  <!-- Titel -->
  <div class="form-group row">
    <label for="input2" class="col-sm-2 col-form-label text-uppercase">Titel</label>

    <div class="col-sm-9">
      <input type="text" id="input2" class="form-control @error('title') is-invalid @enderror" name="title"
        value="{{old('title')}}" required>

      @error('title')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Titel -->


  <!-- Weblink -->
  <div class="form-group row">
    <label for="input9" class="col-sm-2 col-form-label text-uppercase">Banner Link</label>

    <div class="col-sm-9">
      <input type="url" id="input9" class="form-control @error('weblink') is-invalid @enderror" name="weblink"
        value="{{old('weblink')}}">

      @error('weblink')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Weblink -->


  <!-- Date -->
  <div class="form-group row">
    <label class="col-sm-2 col-form-label text-uppercase">Gültig</label>

    <div class="col-sm-4">
      <input type="date" class="form-control @error('valid_from') is-invalid @enderror" name="valid_from"
        value="{{old('valid_from')}}">

      @error('valid_from')
      <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
      @enderror
    </div>

    <div class="col-sm-4">
      <input type="date" class="form-control @error('valid_to') is-invalid @enderror" name="valid_to"
        value="{{old('valid_to')}}">

      @error('valid_to')
      <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
      @enderror
    </div>
  </div>
  <!-- End Date -->

  <!-- Button -->
  <div class="form-group row mt-5">
    <div class="col-sm-7">
      <button type="submit" class="btn btn-danger btn-block text-left">EINTRAG SPEICHERN</button>
    </div>
  </div>
  <!-- End Button -->
</form>
@endsection