@extends('layouts.admin')

@section('content')
<form id="filterForm" action="" class="mb-4">
  <div class="row">
    <div class="col-sm-3 offset-sm-6 mb-3 mb-sm-0">
      <select id="categoriesFilter" class="custom-select" name="type">
        <option value="">E-Mail-Typen</option>
        @foreach($types as $type)
        <option value="{{$type}}" @if(request()->type == $type) selected
          @endif>{{$type}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-3">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Suche" name="title" aria-label="Bannertitel"
          value="{{ request()->title }}" aria-describedby="button-addon2">
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit" id="button-addon2">&#8594;</button>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="table-responsive">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th class="text-uppercase" scope="col">Id</th>
        <th scope="col">E-Mail-Typ</th>
        <th scope="col">E-Mail-Sprache</th>
        <th scope="col">Titel</th>
        <th scope="col">Status</th>
        <th scope="col">Klicks Detailviews</th>
        <th class="text-center text-uppercase" scope="col">BEARBEITEN</th>
        <th class="text-center text-uppercase" scope="col">Löschen</th>
      </tr>
    </thead>
    <tbody>
      @foreach($banners as $banner)
      <tr>
        <td>{{$banner->id}}</td>
        <td>{{$banner->type}}</td>
        <td>{{$banner->lang}}</td>
        <td>{{$banner->title}}</td>
        <td>
          <a href="{{route('email-banner.diactivate', $banner->id)}}">
            <img src="{{asset('img/icons/status-' . $banner->status . '.png')}}" alt="Image Description">
          </a>
        </td>
        <td>{{$banner->getActivityCount('weblink')}}</td>
        <td class="text-center">
          <a href="{{route('email-banner.edit', $banner->id)}}">
            <img src="{{asset('img/icons/edit.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="#" class="delete" data-id="{{ $banner->id }}" data-slug="email-banner" data-toggle="modal"
            data-target="#deleteModal">
            <img src="{{asset('img/icons/delete.png')}}" alt="Image Description">
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="row">
  <div class="col-sm-3 mb-3 mb-sm-3">
    <a class="btn btn-danger btn-block text-left" href="{{route('email-banner.create')}}">EINTRAG ERFASSEN</a>
  </div>

  <div class="col-sm-9">
    {{$banners->links()}}
  </div>
</div>
@endsection