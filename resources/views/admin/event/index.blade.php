@extends('layouts.admin')

@section('content')
<form id="filterForm" action="" class="mb-4">
  <div class="row">
    <div class="col-sm-3 offset-sm-6 mb-3 mb-sm-0">
      <select id="categoriesFilter" class="custom-select" name="category">
        <option value="">All Kategorie</option>
        @foreach($categories as $category)
        <option value="{{$category->id}}" @if(request()->category == $category->id) selected
          @endif>{{$category->name_de}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-3">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Suche" name="title" aria-label="Recipient's username"
          aria-describedby="button-addon2">
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit" id="button-addon2">&#8594;</button>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="table-responsive">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th class="text-uppercase" scope="col">Id</th>
        <th scope="col">Titel</th>
        <th scope="col">Kategorie</th>
        <th scope="col">Klicks Detailviews</th>
        <th scope="col">Anzahl Merkliste</th>
        <th scope="col">Status</th>
        <th class="text-center text-uppercase" scope="col">BEARBEITEN</th>
        <th class="text-center text-uppercase" scope="col">Löschen</th>
      </tr>
    </thead>
    <tbody>
      @foreach($events as $event)
      <tr>
        <td>{{$event->id}}</td>
        <td>{{$event->title}}</td>
        <td>{{$event->categories->name_de}}</td>
        <td>{{$event->getActivityCount('detail')}}</td>
        <td>{{$event->getWatchListCount()}}</td>
        <td>
          <a href="{{route('event.diactivate', $event->id)}}">
            <img src="{{asset('img/icons/status-' . $event->status . '.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="{{route('event.edit', $event->id)}}">
            <img src="{{asset('img/icons/edit.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="#" class="delete" data-id="{{ $event->id }}" data-slug="event" data-toggle="modal"
            data-target="#deleteModal">
            <img src="{{asset('img/icons/delete.png')}}" alt="Image Description">
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="row">
  <div class="col-sm-3 mb-3 mb-sm-3">
    <a class="btn btn-danger btn-block text-left" href="{{route('event.create')}}">EINTRAG ERFASSEN</a>
  </div>

  <div class="col-sm-9">
    {{$events->links()}}
  </div>
</div>
@endsection