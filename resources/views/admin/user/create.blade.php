@extends('layouts.admin')

@section('content')
  <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data">
  @csrf
    <!-- Last Name -->
    <div class="form-group row">
      <label for="input2" class="col-sm-2 col-form-label text-uppercase">Vorname</label>

      <div class="col-sm-7">
        <input type="text" id="input2" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{old('last_name') ? old('last_name') : ''}}" required>

        @error('last_name')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Last Name -->

    <!-- Name -->
    <div class="form-group row">
      <label for="input-name" class="col-sm-2 col-form-label text-uppercase">Name</label>

      <div class="col-sm-7">
        <input type="text" id="input-name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name') ? old('name') : ''}}" required>

        @error('name')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Name -->

    <!-- Email -->
    <div class="form-group row">
      <label for="input-email" class="col-sm-2 col-form-label text-uppercase">Email</label>

      <div class="col-sm-7">
        <input type="email" id="input-email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old('email') ? old('email') : ''}}" required>

        @error('email')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Email -->

    <!-- Password -->
    <div class="form-group row">
      <label for="input-name" class="col-sm-2 col-form-label text-uppercase">Password</label>

      <div class="col-sm-7">
        <input type="password" id="input-password" class="form-control @error('password') is-invalid @enderror" name="password" required value="{{old('password')}}">

        @error('password')
        <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
        @enderror
      </div>
    </div>
    <!-- End Password -->

    <!-- Button -->
    <div class="form-group row mt-5">
      <div class="col-sm-7">
        <div class="d-flex">
          <button type="submit" class="btn btn-danger btn-block text-left mr-2">EINTRAG SPEICHERN</button>
        </div>
      </div>
    </div>
    <!-- End Button -->
  </form>
@endsection
