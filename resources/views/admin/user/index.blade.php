@extends('layouts.admin')

@section('content')
<form id="filterForm" action="" class="mb-4">
  <div class="row">
    <div class="col-sm-3 offset-sm-5 mb-3 mb-sm-0">
      <select id="categoriesFilter" class="custom-select" name="status">
        <option value="">All status</option>
        <option value="true" @if(request()->status === "true") selected @endif>Active</option>
        <option value="false" @if(request()->status === "false") selected @endif>Disabled</option>
      </select>
    </div>

    <div class="col-sm-3">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Suche" name="vorname" aria-label="Recipient's username"
          aria-describedby="button-addon2">
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit" id="button-addon2">&#8594;</button>
        </div>
      </div>
    </div>

    <div class="col-sm-1">
      <a class="btn btn-light" href="{{route('user.export')}}">
        <img src="{{asset('img/icons/export.png')}}" alt="Export">
      </a>
    </div>
  </div>
</form>

<div class="table-responsive">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th class="text-uppercase" scope="col">Id</th>
        <th scope="col">Vorname</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Register Datum</th>
        <th scope="col">Anzahl Merkliste</th>
        <th scope="col">Status</th>
        <th class="text-center text-uppercase" scope="col">BEARBEITEN</th>
        <th class="text-center text-uppercase" scope="col">Löschen</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
      <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->last_name}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->created_at}}</td>
        <td>{{count($user->bucket)}}</td>
        <td>
          <img src="{{asset('img/icons/status-' . $user->status . '.png')}}" alt="Image Description">
        </td>
        <td class="text-center">
          <a href="{{route('user.edit', $user->id)}}">
            <img src="{{asset('img/icons/edit.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="#" class="delete" data-id="{{ $user->id }}" data-slug="user" data-toggle="modal"
            data-target="#deleteModal">
            <img src="{{asset('img/icons/delete.png')}}" alt="Image Description">
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="row">
  <div class="col-sm-3 mb-3 mb-sm-3">
    <a href="{{route('user.create')}}" class="btn btn-danger btn-block text-left">EINTRAG ERFASSEN</a>
  </div>

  <div class="col-sm-9">
    {{$users->links()}}
  </div>
</div>
@endsection