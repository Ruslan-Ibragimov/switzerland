@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-sm-11">
        <img class="img-fluid mb-4" src="{{asset($ad->preview)}}" width="336px" height="234px" alt="Image Description">
    </div>
</div>

<form action="{{route('ad.update', $ad->id)}}" method="post" enctype="multipart/form-data">
    @csrf

    @method('put')

    <!-- Preview -->
    <div class="form-group row">
        <label for="input" class="col-sm-2 col-form-label text-uppercase">Photo preview</label>

        <div class="col-sm-9">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('preview') is-invalid @enderror"
                    id="inputGroupFile01" name="preview" aria-describedby="inputGroupFileAddon01">
                <small class="text-muted">1120X780</small>

                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

                @error('preview')
                <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
                @enderror
            </div>
        </div>
    </div>
    <!-- End Preview -->

    <!-- Firma -->
    <div class="form-group row">
        <label for="input2" class="col-sm-2 col-form-label text-uppercase">Firma</label>

        <div class="col-sm-9">
            <input type="text" id="input2" class="form-control @error('firma') is-invalid @enderror" name="firma"
                value="{{old('firma') ? old('firma') : $ad->firma}}" required>

            @error('firma')
            <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
            @enderror
        </div>
    </div>
    <!-- End Firma -->

    <!-- Weblink -->
    <div class="form-group row">
        <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink</label>

        <div class="col-sm-9">
            <input type="url" id="input9" class="form-control @error('weblink') is-invalid @enderror" name="weblink"
                value="{{old('weblink') ? old('weblink') : $ad->weblink}}">

            @error('weblink')
            <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
            @enderror
        </div>
    </div>
    <!-- End Weblink -->

    <!-- Rubrik -->
    <div class="form-group row">
        <label for="input6" class="col-sm-2 col-form-label text-uppercase">Rubrik</label>

        <div class="col-sm-9">
            <select id="input6" class="custom-select @error('topic_id') is-invalid @enderror" name="topic_id" required>
                <option value=""></option>
                @foreach($categories as $category)
                <option value="{{$category->id}}" @if($ad->topic_id == $category->id) selected
                    @endif>{{$category->name_de}}</option>
                @endforeach
            </select>

            @error('topic_id')
            <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
            @enderror
        </div>
    </div>
    <!-- End Rubrik -->

    <!-- Date -->
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-uppercase">Gültig</label>

        <div class="col-sm-4">
            <input type="date" class="form-control @error('valid_from') is-invalid @enderror" name="valid_from"
                value="{{old('valid_from') ? old('valid_from') : $ad->valid_from}}">

            @error('valid_from')
            <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
            @enderror
        </div>

        <div class="col-sm-4">
            <input type="date" class="form-control @error('valid_to') is-invalid @enderror" name="valid_to"
                value="{{old('valid_to') ? old('valid_to') : $ad->valid_to}}">

            @error('valid_to')
            <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong> </span>
            @enderror
        </div>
    </div>
    <!-- End Date -->

    <!-- Button -->
    <div class="form-group row mt-5">
        <div class="col-sm-7">
            <div class="d-flex">
                <button type="submit" class="btn btn-danger btn-block text-left mr-2">EINTRAG SPEICHERN</button>

                <button class="btn btn-danger" form="deleteForm">
                    <img src="{{asset('img/icons/ebene.png')}}" alt="Image Description">
                </button>
            </div>
        </div>
    </div>
    <!-- End Button -->
</form>

<form id="deleteForm" action="{{route('ad.destroy', $ad->id)}}" method="post">
    @csrf
    @method('delete')
</form>
@endsection
