@extends('layouts.admin')

@section('content')
<form id="filterForm" action="" class="mb-4">
  <div class="row">
    <div class="col-sm-3 offset-sm-3 mb-3 mb-sm-0">
      <select id="regionsFilter" class="custom-select" name="region">
        <option value="">All Regions</option>
        @foreach($regions as $region)
        <option value="{{$region->id}}" @if(request()->region == $region->id) selected
          @endif>{{$region->region_de}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-3  mb-3 mb-sm-0">
      <select id="categoriesFilter" class="custom-select" name="category">
        <option value="">All Kategorie</option>
        @foreach($categories as $category)
        <option value="{{$category->id}}" @if(request()->category == $category->id) selected
          @endif>{{$category->name_de}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-3">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Suche" name="title" aria-label="Recipient's username"
          aria-describedby="button-addon2">
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit" id="button-addon2">&#8594;</button>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="table-responsive">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th class="text-uppercase" scope="col">Id</th>
        <th scope="col">Titel</th>
        <th scope="col">Region</th>
        <th scope="col">Kategorie</th>
        <th scope="col">Klicks Detailviews</th>
        <th scope="col">Anzahl Merkliste</th>
        <th scope="col">Status</th>
        <th class="text-center text-uppercase" scope="col">BEARBEITEN</th>
        <th class="text-center text-uppercase" scope="col">Löschen</th>

      </tr>
    </thead>
    <tbody>
      @foreach($places as $place)
      <tr>
        <td>{{$place->id}}</td>
        <td>{{$place->title}}</td>
        <td>{{$place->region->region_de}}</td>
        <td>{{$place->categories->name_de}}</td>
        <td>{{$place->getActivityCount('detail')}}</td>
        <td>{{$place->getWatchListCount()}}</td>
        <td>
          <a href="{{route('place.diactivate', $place->id)}}">
            <img src="{{asset('img/icons/status-' . $place->status . '.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="{{route('place.edit', $place->id)}}">
            <img src="{{asset('img/icons/edit.png')}}" alt="Image Description">
          </a>
        </td>
        <td class="text-center">
          <a href="#" class="delete" data-id="{{ $place->id }}" data-slug="place" data-toggle="modal"
            data-target="#deleteModal">
            <img src="{{asset('img/icons/delete.png')}}" alt="Image Description">
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="row">
  <div class="col-sm-3 mb-3 mb-sm-3">
    <a class="btn btn-danger btn-block text-left" href="{{route('place.create')}}">EINTRAG ERFASSEN</a>
  </div>

  <div class="col-sm-9">
    {{$places->links()}}
  </div>
</div>
@endsection