@extends('layouts.admin')

@section('content')
<div class="row">
  <div class="col-sm-11">
    <img class="img-fluid mb-4" src="{{asset($place->preview)}}" alt="Image Description">
  </div>
</div>

<form action="{{route('place.update', $place->id)}}" method="post" enctype="multipart/form-data">
  @csrf

  @method('put')

  <!-- Preview -->
  <div class="form-group row">
    <label for="input" class="col-sm-2 col-form-label text-uppercase">Photo preview</label>

    <div class="col-sm-9">
      <div class="custom-file">
        <input type="file" class="custom-file-input @error('preview') is-invalid @enderror" id="inputGroupFile01"
          name="preview" aria-describedby="inputGroupFileAddon01">
        <small class="text-muted">1120X570</small>

        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>

        @error('preview')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>
  </div>
  <!-- End Preview -->

  <!-- Video link -->
  <div class="form-group row">
    <label for="input1" class="col-sm-2 col-form-label text-uppercase">Video link</label>

    <div class="col-sm-9">
      <input type="url" id="input1" class="form-control @error('video_link') is-invalid @enderror" name="video_link"
        value="{{old('video_link') ? old('video_link') : $place->video_link}}">

      @error('video_link')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Video link -->

  <!-- Titel -->
  <div class="form-group row">
    <label for="input2" class="col-sm-2 col-form-label text-uppercase">Titel</label>

    <div class="col-sm-9">
      <input type="text" id="input2" class="form-control @error('title') is-invalid @enderror" name="title"
        value="{{old('title') ? old('title') : $place->title}}" required>

      @error('title')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Titel -->

  <!-- Adresse -->
  <div class="form-group row">
    <label for="input3" class="col-sm-2 col-form-label text-uppercase">Adresse</label>

    <div class="col-sm-9">
      <textarea id="input3" class="form-control @error('address') is-invalid @enderror" name="address"
        required>{{old('address') ? old('address') : $place->address}}</textarea>

      @error('address')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Adresse -->

  <!-- Google -->
  <div class="form-group row">
    <label for="input4" class="col-sm-2 col-form-label text-uppercase">Google</label>

    <div class="col-sm-9">
      <input type="text" id="input4" class="form-control @error('google') is-invalid @enderror" name="google"
        value="{{old('google') ? old('google') : $place->google}}" required>

      @error('google')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Google -->

  <!-- Region -->
  <div class="form-group row">
    <label for="input5" class="col-sm-2 col-form-label text-uppercase">Region</label>

    <div class="col-sm-9">
      <select id="input5" class="custom-select @error('region_id') is-invalid @enderror" name="region_id" required>
        <option value=""></option>
        @foreach($regions as $region)
        <option value="{{$region->id}}" @if($place->region_id == $region->id) selected @endif>{{$region->region_de}}
        </option>
        @endforeach
      </select>

      @error('region_id')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Region -->

  <!-- Rubrik -->
  <div class="form-group row">
    <label for="input6" class="col-sm-2 col-form-label text-uppercase">Rubrik</label>

    <div class="col-sm-9">
      <select id="input6" class="custom-select @error('category_id') is-invalid @enderror" name="category_id" required>
        <option value=""></option>
        @foreach($categories as $category)
        <option value="{{$category->id}}" @if($place->category_id == $category->id) selected
          @endif>{{$category->name_de}}</option>
        @endforeach
      </select>

      @error('category_id')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Rubrik -->

  <!-- BeschriebDeutsch -->
  <div class="form-group row">
    <label for="input7" class="col-sm-2 col-form-label text-uppercase">BeschriebDeutsch</label>

    <div class="col-sm-9">
      <textarea id="input7" class="form-control @error('description_de') is-invalid @enderror" rows="4"
        name="description_de"
        required>{{old('description_de') ? old('description_de') : $place->description_de}}</textarea>

      @error('description_de')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End BeschriebDeutsch -->

  <!-- DescriptionEnglish -->
  <div class="form-group row">
    <label for="input8" class="col-sm-2 col-form-label text-uppercase">DescriptionEnglish</label>

    <div class="col-sm-9">
      <textarea id="input8" class="form-control @error('description_en') is-invalid @enderror" rows="4"
        name="description_en"
        required>{{old('description_en') ? old('description_en') : $place->description_en}}</textarea>

      @error('description_en')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End DescriptionEnglish -->

  <!-- Weblink -->
  <div class="form-group row">
    <label for="input9" class="col-sm-2 col-form-label text-uppercase">Weblink</label>

    <div class="col-sm-9">
      <input type="url" id="input9" class="form-control @error('weblink') is-invalid @enderror" name="weblink"
        value="{{old('weblink') ? old('weblink') : $place->weblink}}">

      @error('weblink')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Weblink -->

  <!-- Facebook -->
  <div class="form-group row">
    <label for="input10" class="col-sm-2 col-form-label text-uppercase">Facebook</label>

    <div class="col-sm-9">
      <input type="url" id="input10" class="form-control @error('facebook') is-invalid @enderror" name="facebook"
        value="{{old('facebook') ? old('facebook') : $place->facebook}}">

      @error('facebook')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Facebook -->

  <!-- Instagram -->
  <div class="form-group row">
    <label for="input11" class="col-sm-2 col-form-label text-uppercase">Instagram</label>

    <div class="col-sm-9">
      <input type="url" id="input11" class="form-control @error('instagram') is-invalid @enderror" name="instagram"
        value="{{old('instagram') ? old('instagram') : $place->instagram}}">

      @error('instagram')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Instagram -->

  <!-- App Tipp -->
  <div class="form-group row">
    <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (IOS)</label>

    <div class="col-sm-9">
      <input type="url" id="input12" class="form-control @error('app_tipp') is-invalid @enderror" name="app_tipp"
        value="{{old('app_tipp') ? old('app_tipp') : $place->app_tipp}}">

      @error('app_tipp')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End App Tipp -->

  <!-- App Tipp Android -->
  <div class="form-group row">
    <label for="input12" class="col-sm-2 col-form-label text-uppercase">App Tipp (Android)</label>

    <div class="col-sm-9">
      <input type="url" id="input12" class="form-control @error('app_tipp_android') is-invalid @enderror"
        name="app_tipp_android"
        value="{{old('app_tipp_android') ? old('app_tipp_android') : $place->app_tipp_android}}">

      @error('app_tipp_android')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End App Tipp -->

  <!-- Telefon -->
  <div class="form-group row">
    <label for="input13" class="col-sm-2 col-form-label text-uppercase">Telefon</label>

    <div class="col-sm-9">
      <input type="tel" id="input13" class="form-control @error('phone') is-invalid @enderror" name="phone"
        value="{{old('phone') ? old('phone') : $place->phone}}">

      @error('phone')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
  <!-- End Telefon -->

  <!-- Button -->
  <div class="form-group row mt-5">
    <div class="col-sm-7">
      <div class="d-flex">
        <button type="submit" class="btn btn-danger btn-block text-left mr-2">EINTRAG SPEICHERN</button>

        <button class="btn btn-danger" form="deleteForm">
          <img src="{{asset('img/icons/ebene.png')}}" alt="Image Description">
        </button>
      </div>
    </div>
  </div>
  <!-- End Button -->
</form>

<form id="deleteForm" action="{{route('place.destroy', $place->id)}}" method="post">
  @csrf
  @method('delete')
</form>
@endsection