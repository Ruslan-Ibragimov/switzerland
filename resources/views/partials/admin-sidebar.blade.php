<div class="admin-sidebar d-none d-md-block">
    <div class="p-4 mb-4">
        <img src="{{asset('img/logo/logo.png')}}" width="111px" height="59px" alt="Image Description">
    </div>

    <ul class="nav list-group">
        <li class="nav-item">
            <a class="nav-link" href="#"
                onclick="event.preventDefault();document.getElementById('logout-form').submit();">Abmelden</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
        <li class="nav-item" data-name="author">
            <a class="nav-link" href="{{route('author.index')}}">Authors</a>
        </li>

        <li class="nav-item" data-name="place">
            <a class="nav-link" href="{{route('place.index')}}">Must see {{request()->is(route('place.index'))}}</a>
        </li>
        <li class="nav-item" data-name="event">
            <a class="nav-link" href="{{route('event.index')}}">Events & Tickets</a>
        </li>
        <li class="nav-item" data-name="tip">
            <a class="nav-link" href="{{route('tip.index')}}">Tips from Locals</a>
        </li>
        <li class="nav-item" data-name="service">
            <a class="nav-link" href="{{route('service.index')}}">Services</a>
        </li>
        <li class="nav-item" data-name="shop">
            <a class="nav-link" href="{{route('shop.index')}}">Souvenir Shop</a>
        </li>
        <li class="nav-item" data-name="user">
            <a class="nav-link" href="{{route('user.index')}}">Bucket list users</a>
        </li>
        <li class="nav-item " data-name="ad">
            <a class="nav-link" href="{{route('ad.index')}}">Advertising banner</a>
        </li>
        <li class="nav-item " data-name='email-banner'>
            <a class="nav-link" href="{{route('email-banner.index')}}">Email banner</a>
        </li>
    </ul>
</div>