<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Theme Styles -->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet">
</head>

<body>
    @include('layouts.loader')
    <div class="row no-gutters">
        <div class="col-md-2">
            @include('partials.admin-sidebar')
        </div>

        <div class="col-md-10">
            <div id="app">
                <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm d-block d-md-none">
                    <div class="container">
                        <a class="navbar-brand d-sm-none" href="{{ url('/') }}">
                            {{ config('app.name', 'Switzerland') }}
                        </a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="#"
                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">Abmelden</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                                <li class="nav-item" data-name='author'>
                                    <a class="nav-link" href="{{route('author.index')}}">Authors</a>
                                </li>
                                <li class="nav-item" data-name='place'>
                                    <a class="nav-link" href="{{route('place.index')}}">Must see
                                        {{request()->is(route('place.index'))}}</a>
                                </li>
                                <li class="nav-item" data-name='event'>
                                    <a class="nav-link" href="{{route('event.index')}}">Events & Tickets</a>
                                </li>
                                <li class="nav-item" data-name='tip'>
                                    <a class="nav-link" href="{{route('tip.index')}}">Tips from Locals</a>
                                </li>
                                <li class="nav-item" data-name='service'>
                                    <a class="nav-link" href="{{route('service.index')}}">Services</a>
                                </li>
                                <li class="nav-item" data-name='shop'>
                                    <a class="nav-link" href="{{route('shop.index')}}">Souvenir Shop</a>
                                </li>
                                <li class="nav-item" data-name='user'>
                                    <a class="nav-link" href="{{route('user.index')}}">Bucket list users</a>
                                </li>
                                <li class="nav-item " data-name='ad'>
                                    <a class="nav-link" href="{{route('ad.index')}}">Advertising banner</a>
                                </li>
                                <li class="nav-item " data-name='email-banner'>
                                    <a class="nav-link" href="{{route('email-banner.index')}}">Email banner</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <main class="admin-content">
                    <div class="container py-5">
                        @yield('content')
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Möchten Sie dies wirklich löschen?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">Schließen</button>
                                    <button type="button" class="delete-item btn btn-danger">Löschen</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script type="text/javascript">
        document.body.style.setProperty('overflow','hidden')
        $(document).ready(function () {
            setTimeout(() => {
                $('.loader').addClass('disactive')
                document.body.style.removeProperty('overflow');
            }, 1000);
        });
    </script>
    <script>
        $(document).on('click', '.delete-item', function(e){
            $('.loader').removeClass('disactive')
            let id = e.currentTarget.dataset.id;
            let route = e.currentTarget.dataset.route + '/' + id;
            let method = 'delete';
            let csrf ='{{ csrf_token() }}';
            $.ajax({
                type: method,
                url: route,
                data: {'_token': csrf},
                dataType: "json",
                success: function (response) {
                    location.reload();
                },
                error:function(xhr){
                    location.reload();
                }
            });
        });
                
        let showPopup = function (id,route,method = 'delete') {  
            let btn = document.querySelector('.delete-item');
            btn.dataset.id = id;
            btn.dataset.route = route;
            $('#deleteModal .modal-title').html(`Artikel  <b>#${id}</b>`);
        }

    </script>
    <script>
        $(document).ready(function () {
        bsCustomFileInput.init()

        $('#categoriesFilter').on('change', function () {
            // debugger;
          document.forms['filterForm'].submit();
        });
        $('#regionsFilter').on('change', function () {
          document.forms['filterForm'].submit();
        });
        let url = window.location.href.split("/")[3];
        url = url.split('?')[0];
        $('.nav-item[data-name='+url+']').addClass('active')
        $('.delete').click(function (e) { 
        let id = e.currentTarget.dataset.id;
        let route = e.currentTarget.dataset.slug;
        showPopup(id,route);
        });

      })
    </script>
    @yield('javascript')
</body>

</html>