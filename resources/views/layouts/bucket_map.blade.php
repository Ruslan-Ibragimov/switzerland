<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/leaflet.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="map px-0 col-md-9">
                <div class="toggler">
                    <img src="{{ asset('img/icons/toggler.svg') }}" alt="">
                </div>
                @yield('content')
            </div>
            <div class="map-navbar col-md-3">
                <div class="d-flex py-4 align-items-center justify-content-start">
                    <img src="{{ asset('img/logo/logo.png') }}" alt="">
                </div>
                <div class="info mb-5">
                    <h5>MY BUCKET LIST</h5>
                </div>
                <ul>
                    @php
                    $index = 0
                    @endphp
                    @foreach ($data->geo_info as $slug => $item)
                    @php
                    $index++
                    @endphp
                    <li class="title">{{ $slug }}</li>
                    @foreach ($item as $key => $info)
                    <li data-guid="{{ $slug.'_'.$info->id }}"
                        class="{{ $index == 1 && $key == 0 ?'active' : '' }} link">{{ $info->title }}</li>
                    @endforeach

                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <style>
        .map {}

        .map-navbar {
            background-color: #2e2825;
            overflow-y: auto;
            height: 100%;
            transition: all .3s linear;
        }

        .map-navbar ul {
            margin: 0;
            padding: 0;
            list-style: none;
            color: #fff;
        }

        .map-navbar ul .title {
            text-transform: uppercase;
            font-weight: bold;
            margin: 8px 0;
            padding: 2px;
            font-size: 20px;
            border-bottom: 2px solid #fff;
            position: relative;
        }

        /* .map-navbar ul .title::before {
            content: '';
            width: 100%;
            height: 2px;
            background: #c6001e;
            position: absolute;
            left: 0;
            bottom: -4px;
        } */

        .info {
            text-align: left;
            color: #fff;
        }

        .info h5 {
            font-size: 20px;
            font-weight: bold;
        }

        .map-navbar ul .link {
            cursor: pointer;
            padding: 4px;
            transition: all .3s linear;
        }

        .active {
            color: #c6001e;
        }

        .map-navbar ul .link:hover {
            color: #c6001e;
        }

        /* @media only screen and (max-width: 600px) {
            body {
                background-color: lightblue;
            }
        } */
        .navbar-active {
            right: 0 !important;
            display: block !important;
        }

        .toggler {
            cursor: pointer;
            position: fixed;
            top: 20px;
            right: 20px;
            z-index: 111111;
            padding: 10px;
            background: #fff;
            border-radius: 5px;
            background: #e3342f;
            box-shadow: 0px 2px 9px 0px #000000db;
            display: none;
        }

        .toggler-diactive {
            display: none !important;
        }

        @media(max-width:768px) {
            .toggler {
                display: block;
            }

            .map-navbar {
                position: absolute;
                display: none;
                right: -100%;
                top: 0;
                width: 100%;
                height: 100%;
                z-index: 111111;
                background-color: #2e2825d9;
            }
        }
    </style>

    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/leaflet.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    @yield('javascript')
</body>

</html>