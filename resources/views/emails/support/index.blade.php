@extends('emails.layouts.email')

@section('content')
<div>
    <div class="item mb-4">
        <p class="font-weight-bold mb-1">Grüezi Switzerland - Support</p>
        <p class="font-weight-bold mb-1">Grüezi Switzerland</p>
    </div>

    <div class="mb-4">
        <img width="200px" src="{{ $message->embed(asset('img/logo/gslogo.png')) }}">
    </div>
    <div class="mb-4">
        <p class="mx-0 font-weight-bold">Subject: Grüezi Switzerland - User's Request</p>
    </div>
    <div class="mb-4">
        <p>
            <strong>First name: - </strong> {{ $data['first_name'] }}<br>
            <strong>Last name: - </strong> {{ $data['last_name'] }}<br>
            <strong>Email: - </strong> {{ $data['email'] }}<br>
            <strong>Smartphone: - </strong> {{ $data['smartphone'] }}<br>
            <strong>Operating system: - </strong> {{ $data['system'] }}<br>
        </p>
        <p>
            <strong>Request:</strong><br>
            {{ $data['note'] }}
            <br>
        </p>
    </div>
    <div>
        <p>© {{ date('Y') }} Grüezi Switzerland. All rights reserved.</p>
        <p class="my-0">{{ __('general.footer') }}</p>
        <p class="my-0"><a href="www.spherix.ch">www.spherix.ch</a></p>
    </div>
</div>
@endsection