@extends('emails.layouts.email')

@section('content')
<div>
    <div class="item mb-4">
        <p class="font-weight-bold mb-1">Grüezi Switzerland - Reset Password</p>
        <p class="font-weight-bold mb-1">Grüezi Switzerland</p>
    </div>

    <div class="mb-4">
        <img width="200px" src="{{ $message->embed(asset('img/logo/gslogo.png')) }}">
    </div>
    <div class="mb-4">
        <p class="mx-0 font-weight-bold">Subject: Grüezi Switzerland - Reset Password</p>
    </div>
    <div class="mb-4">
        <p>
            Hello {{ $user->name }} {{ $user->last_name }}<br>
            You have requested your access data for the "Grüezi Switzerland" App.<br>
            Please note that this is an automatic e-mail address is not used personally by our team. Your
            answer will not be read
            or processed.<br>
        </p>
    </div>
    <div>
        <p>Many thanks and best regards <br> Support desk</p>
    </div>
    <div class="mb-4">
        <a href="{{ $url }}" class="btn btn-danger btn-sm d-infline-flex text-left">Reset Password</a>
    </div>
    <div class="mb-4">
        <p>
            This password reset link will expire in 60 minutes counting.
            If you have not requested a password reset, no further action is required
        </p>
    </div>

    <div class="mb-4">
        <p>
            If you have trouble clicking the "Reset Password" button, copy and paste the following URL into
            your web browser:
        </p>
        <p>
            <a class="color-info" href="{{ $url }}">{{$url}} </a>
        </p>
    </div>
    <div>
        <p>© {{ date('Y') }} Grüezi Switzerland. All rights reserved.</p>
        <p class="my-0">{{ __('general.footer') }}</p>
        <p class="my-0"><a href="www.spherix.ch">www.spherix.ch</a></p>
    </div>
</div>
@endsection