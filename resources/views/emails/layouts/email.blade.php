<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Switzerland</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .center {
            max-width: 750px;
            margin: 10px auto;
            padding: 20px;
        }

        p {
            color: #636b6f !important;
        }

        .bgwhite {
            padding: 20px;
            background: #fff;
            border-radius: 5px;
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="center">
            <div class="bgwhite">
                @yield('content')
            </div>
        </div>
    </div>
</body>

</html>