@extends('emails.layouts.email')

@section('content')
<div>
    <div class="mb-4">
        <img width="200px" src="{{ $message->embed(asset('img/logo/gslogo.png')) }}">
    </div>
    <div class="item mb-4">
        <p class="font-weight-bold mb-1">{{ __('general.bucket_title') }}</p>
    </div>

    <div class="mb-4">
        <p>
            {{ __('general.greating') }} {{ $user->name }} {{ $user->last_name }}!<br>
            {{ __('general.bucket_info') }}
        </p>
    </div>
    <div class="mb-4">
        <a href="{{ $url }}" class="btn btn-danger btn-sm d-infline-flex text-left"> {{ __('general.travel_map') }}</a>
    </div>


    <div class="mb-4">
        <p>{{ __('general.btn-issue') }}</p>
        <p>
            <a class="color-info" href="{{ $url }}">{{$url}} </a>
        </p>
    </div>

    @isset($banner->id)
    <div class="mb-3">
        <a href="{{route('redirect',['token' => $banner->redirect])}}">
            <p class="font-weight-bold mb-1">{{  $banner->title }}</p>
        </a>
        <a href="{{route('redirect',['token' => $banner->redirect])}}">
            <p>
                <img style="width: 100%" src="{{ $message->embed(asset(url('/') . '/' . $banner->img )) }}">
            </p>
        </a>
    </div>
    @endif

    <div>
        <p>© {{ date('Y') }} Grüezi Switzerland. All rights reserved.</p>
        <p class="my-0">{{ __('general.footer') }}</p>
        <p class="my-0"><a href="www.spherix.ch">www.spherix.ch</a></p>
    </div>
</div>
@endsection