<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'not_active' => 'This account is not activated.Please confirm your email',
    'logout' => 'Successfully logged out',
    'email_failure' => 'Something went wrong while sending email',
    'reset_pass' => 'Password reset link has been sent to :email',
    'activate_link' => 'Account activation  link has been sent to :email',
    'not_users_bucket' => 'This bucket is not belonged to this user',
    'bucket_delete' => 'Successfully deleted from your bucket list',
    'succes_request' => 'Your request has been successfully sent!',
    'undefined_user' => 'There is no such user with this email',
    'empty_bucket' => 'Your bucket list is empty',
    'bucket_exported' => 'Your bucket list has been exported to :email',
    'unique_email' => 'The email has already been taken.',
    'check_email' => 'There is no such user with this email'



];
