<?php

return [
    'bucket_title' => 'Your "Grüezi Switzerland" travel map',
    'greating' => 'Hello',
    'travel_map' => 'Travel map',
    'bucket_info' => 'In the following we send you the link to your personal travel map:',
    'btn-issue' => 'If you have trouble clicking the button, copy and paste the following URL into your web browser:',
    'footer' => 'A project of Spherix AG . CH-9000 St.Gallen',
    'activation_title' => 'Your "Grüezi Switzerland" Account activation',
    'activation_info' => 'To activate your account please click the button below to verify your email address:',
    'activate_account' => 'Activate Account',
    // 'activation_subject' => 'Grüezi Switzerland - Account activation',
    // 'bucket_subject' => 'Grüezi Switzerland - Bucket list ',
    'activation_subject' => 'Account activation',
    'bucket_subject' => 'Bucket list ',
    'reset_password' => 'Passwort zurücksetzen'


];
