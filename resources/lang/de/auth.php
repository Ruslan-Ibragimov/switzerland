<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Diese Kombination aus Zugangsdaten wurde nicht in unserer Datenbank gefunden',
    'throttle' => 'Zu viele Loginversuche. Versuchen Sie es bitte in :seconds Sekunden nochmal',
    'not_active' => 'Dieses Konto ist nicht aktiviert. Bitte bestätigen Sie Ihre E-Mail',
    'logout' => 'Erfolgreich abgemeldet',
    'email_failure' => 'Beim Senden einer E-Mail ist ein Fehler aufgetreten',
    'reset_pass' => 'Der Link zum Zurücksetzen des Passworts wurde an :email gesendet',
    'activate_link' => 'Der Link zur Konto-Aktivierung wurde an :email gesendet',
    'not_users_bucket' => 'Dieser Bucket gehört nicht diesem Benutzer',
    'bucket_delete' => 'Erfolgreich aus Ihrer Bucket-Liste gelöscht',
    'succes_request' => 'Ihre Anfrage wurde erfolgreich gesendet!',
    'undefined_user' => 'Es gibt keinen solchen Benutzer mit dieser E-Mail',
    'empty_bucket' => 'Ihre Bucket-Liste ist leer',
    'bucket_exported' => 'Ihre Bucket List wurde an :email übermittelt.',
    'unique_email' => 'Die E-Mail-Adresse wird bereits verwendet.',
    'check_email' => 'Es gibt keinen solchen Benutzer mit dieser E-Mail'



];
