<?php

return [
    'bucket_title' => 'Ihre „Grüezi Switzerland“ Reisekarte',
    'greating' => 'Hallo',
    'travel_map' => 'Reisekarte',
    'bucket_info' => 'Nachfolgend senden wir Ihnen den Link zu Ihrer persönlichen Reisekarte:',
    'btn-issue' => 'Falls der Button nicht funktioniert, können Sie die Reisekarte auch über nachfolgenden Link aufrufen. Kopieren Sie diesen im Domain-Feld Ihres Browsers.',
    'footer' => 'Ein Projekt der Spherix AG . CH-9000 St.Gallen',
    'activation_title' => 'Ihre „Grüezi Switzerland“ Konto-Aktivierung',
    'activation_info' => 'Um Ihr „Bucket List“ Konto zu aktivieren und Ihre E-Mail zu bestätigen, bitten wir Sie, nachfolgenden Button zu klicken.',
    'activate_account' => 'Konto aktivieren',
    // 'activation_subject' => 'Grüezi Switzerland - Konto-Aktivierung',
    // 'bucket_subject' => 'Grüezi Switzerland - Bucket list',
    'activation_subject' => 'Konto-Aktivierung',
    'bucket_subject' => 'Bucket list',
    'reset_password' => 'Passwort zurücksetzen'





];
